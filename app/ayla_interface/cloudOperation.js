import commConfig from '../ayla_interface/comm.config';
import {AppStorage} from '../ayla_interface/app-storage.service';
import AsyncStorage from '@react-native-community/async-storage';
import aylaService from './ayla.service';
import { now } from 'moment';
import moment from 'moment';
import  {DeviceEventEmitter} from 'react-native';
import iotTranslation from './iotTranslation';
import {save, load } from "../utils/storage"

const TOKEN_KEY = 'userToken';
let DSN='AC000W017943876'; // AC000W018141402, 
const MESSAGE_OPEN_LID=1000;
const MESSAGE_CLOSE_LID=1001;
const COOKING_STEPS_CHANNEL = "cookingSteps"
const MESSAGE_CHANNEL  ="message"
const MEESSAGE_IOT_DEBUG="iotDebug"
//progress: [{val: 8, time: 60, temp: 102, target: 145]}
//DeviceEventEmitter.emit('xxxName’,param);

//optional from getNow .not consider
global.aylaConfig = commConfig.ayla_us_config;
let readFlagMap = new Map();
let preheating_animation = 0;
let currentTemp=0;
let targetTemp=0;
let countDown=-1;
let tempTimeout   ;
let loginStatus = false;
let preheatBegin=false;
var COOK_STEP = {
    "waiting":0,
    "steak_select":1,
    "steak_ack":2,
    "preheating":3,
    "steak_lid":4,
    "steak_flip":5,
    "remove_steak":6,
    "cauliflower_select":7,
    "cauliflower_ack":8,
    "cauliflower_cook":9,
    "cauliflower_flip":10,
    "add_salsa":11,
    "done":12
  }

// enum COOK_STEP {
//     waiting,
//     steak_select,
//     steak_ack,
//     preheating,
//     steak_lid,
//     steak_flip,
//     remove_steak,
//     cauliflower_select,
//     cauliflower_ack,
//     cauliflower_cook,
//     cauliflower_flip,
//     add_salsa,
//     done
//   }

//range from 1-8
const getPreheatingProgress = ()=>{
  return preheating_animation;
}
//current and target

const getTemp = () =>
{
   return {currentTemp,targetTemp};
}
const switchDSN = (dsn) =>
{
   DSN=dsn;
   console.log("switch DSN :"+ dsn);
}
const getCountDown=()=>
{
  return countDown ;
}

let now_step = COOK_STEP.waiting;//not use AppStorage
const setNowStep = (step)=>{
    //AppStorage.setStep(step.toString());
    now_step = step;
    console.log("set now step:" + step );
}
const getNowStep = () => {
    //const nowStep =  AppStorage.getStep();
    console.log("get now step:" + now_step );
    return now_step;
}
//##########for sample test
// const needReadTemp = (data_key,data)=>{
//       if(readFlagTempMap.has(data_key))
//       {
//         return false;
//       } 
//       else if(data!=="")
//       {
//         readFlagTempMap.set(data_key,"");
//         console.log("new message data_key: " + data_key + " value:" +data)
//         return true;
//       }
//       return false ;
// }
const needRead = (data_key,data)=>{
  if(readFlagMap.has(data_key))
  {
    return false;
  } 
  else if(data!=="")
  {
    readFlagMap.set(data_key,"");
    console.log("new message data_key: " + data_key + " value:" +data)
    return true;
  }
  return false ;
}
const loginCloud  = async () => {
  
  if(loginStatus===true)
  {
    console.log("already login");
    return ;
  }
  const loadDsnFunc = async () => {
    const data = await load("CURRENT_DSN")
    console.log(`current dsn`, JSON.stringify(data))
    if (data && data.dsn) {
       DSN = data.dsn;
    }
  }
  loadDsnFunc();
  global.aylaConfig = commConfig.ayla_us_config ;
  const res = await aylaService.signIn(commConfig.ayla_us_config.email, commConfig.ayla_us_config.password);
    // console.log('res => ', res);
    if (res.error) {
      console.log("error,res.error");
      loginStatus = false;
    } else if (res.access_token) {
      let userToken = res;
      console.log(`success userToken: ${JSON.stringify(userToken)}`);
      await AsyncStorage.setItem(TOKEN_KEY, JSON.stringify(userToken));
      loginStatus = true;
      const restdata = `{ "st":"none"}`;
      setPropertyValue(restdata,"PbInput")
      const outputTimeout = setInterval(() => {
        readCloudMessage();
    }, 500);
     
    const heatingDelay = setTimeout(() => {
               
      tempTimeout = setInterval(() => {
        readCloudMessageTemp();
    }, 500);

   }, 200); 

   const warngDelay = setTimeout(() => {
               
    const warnTimeout = setInterval(() => {
      readWarnMessage();
  }, 500);

 }, 200); 

  };
}
// const getPropertyValue =async (property_name) => {
//     aylaService
//       .getPropertiesByPropertyName(DSN, property_name)
//       .then(result => {
//         if (result && result.property) {
//           const created_at_since_date = moment()
//             .utc()
//             .subtract(60, 'seconds')
//             .format();
//           const data_updated_at = result.property.data_updated_at;
//           if (data_updated_at >= created_at_since_date) {
//             const output = result.property.value;
//             console.log("get "+ property_name + ":"+getPropertyValue + "data_updated_at:" +data_updated_at + ":value" + output );
//             return output;
//           }
          
//           return "error" ;
//         }
//       })
//       .catch(_error => {
//         console.log('getPropertiesByPropertyName', error);
//       });
//   };
  const setPropertyValue =(data,property_name) => {

      return new Promise((resolve, reject) => {
        aylaService
        .sendPbInput(DSN, data,property_name)
        .then(res => {
          console.log('sendInput.res => ', res);
          //alert("send successfully "+ property_name + ":"+data)
        })
        .catch(err => {
          reject(err);
          alert("send error")
        });

      });
  };
//step cloud operation

//set recipe
const setRecipe =(name,med,thickniss,time,temp) => {      
      if(loginStatus === false)
      {
        console.log("you need login first,login now")
        loginCloud();
      }
    
      const data = `{\"recipe\": {\"name\":\"${name}\", \"doneness\":\"${med}\",\"thickniss\":${thickniss},\"time\":${time}, \"temp\": ${temp}}}`
      
      setPropertyValue(data,"PbInput");
      DeviceEventEmitter.emit(MEESSAGE_IOT_DEBUG,{'name':'PbInput','src':data,"dst":"none"});
      if(name==="rib eye" && med==="none" )
      {
          setNowStep(COOK_STEP.steak_select);
      }
      else if(name === "cauliflower" && med==="na")
      {
          setNowStep(COOK_STEP.cauliflower_select);
      }
      else if(name==="rib eye" )
      {
          setAddSteak()
      }

}
const setWifi =(joinflag) => {      
  if(loginStatus === false)
  {
    console.log("you need login first,login now")
    loginCloud();
  }
  data = `{\"st\":\"connected\" }`;
  if(joinflag===false)
  {
    data = `{\"st\":\"disconnected\" }`;
  }
  
  setPropertyValue(data,"PbInput");
}
const setPairing =() => {  
  data = `{\"st\":\"pairing\" }`;    
  setPropertyValue(data,"PbInput");
}
//
//read output  ;
const readCloudMessage = () => {
    aylaService
      .getPropertiesByPropertyName(DSN, 'Pboutput')
      .then(result => {
        if (result && result.property) {
          const created_at_since_date = moment()
            .utc()
            .subtract(60, 'seconds')
            .format();
          const data_updated_at = result.property.data_updated_at;
          if (data_updated_at >= created_at_since_date) {
            if(needRead(data_updated_at+result.property.value,result.property.value))
            {
              console.log("read pbOuput begin")
              DeviceEventEmitter.emit(MEESSAGE_IOT_DEBUG,{'name':'PbOutput','src':result.property.value,"dst":"none"});
              const jsonValue =  JSON.parse(result.property.value);
              console.log("read pbOuput end")
              parseOutput(jsonValue);
            }
            else{
              //console.log("old message data_updated_at:" + data_updated_at + ",created_at_since_date:"+created_at_since_date)
            }

          }
        }
      })
      .catch(error => {
         console.log('readCloudMessage error :', error);
      });
  };
  const readCloudMessageTemp = () => {
    aylaService
      .getPropertiesByPropertyName(DSN, 'ProbeTemp')
      .then(result => {

        if (result && result.property) {
        const created_at_since_date = moment()
            .utc()
            .subtract(60, 'seconds')
            .format();
          const data_updated_at = result.property.data_updated_at;
          if (data_updated_at >= created_at_since_date) {
            if(needRead(data_updated_at+result.property.value,result.property.value))
            {
              DeviceEventEmitter.emit(MEESSAGE_IOT_DEBUG,{'name':'ProbeTemp','src':result.property.value,"dst":"none"});
              const jsonValue =  JSON.parse(result.property.value);
              parseTemp(jsonValue)
            }
          }
          else
          {
            //console.log("old message data_updated_at:" + data_updated_at + ",created_at_since_date:"+created_at_since_date)
          }
        }
      })
      .catch(error => {
         console.log('readtemp error :', error);
      });
  };
  const readWarnMessage = () => {
    aylaService
      .getPropertiesByPropertyName(DSN, 'Warn')
      .then(result => {

        if (result && result.property) {
        const created_at_since_date = moment()
            .utc()
            .subtract(60, 'seconds')
            .format();
          const data_updated_at = result.property.data_updated_at;
          if (data_updated_at >= created_at_since_date) {
            if(needRead(data_updated_at+result.property.value,result.property.value))
            {
              DeviceEventEmitter.emit(MEESSAGE_IOT_DEBUG,{'name':'Warn','src':result.property.value,"dst":"none"});
              const jsonValue =  JSON.parse(result.property.value);
              parseWarn(jsonValue)
            }
          }
          else
          {
            //console.log("old message data_updated_at:" + data_updated_at + ",created_at_since_date:"+created_at_since_date)
          }
        }
      })
      .catch(error => {
         console.log('readtemp error :', error);
      });
  };
const sendEvent  = (param)  =>{

  console.log("send event param"+JSON.stringify(param));
  DeviceEventEmitter.emit(COOKING_STEPS_CHANNEL,param);

}
const sendWarn  = (code)  =>{

  data={status:code}
  console.log("send warn param"+JSON.stringify(data));
  DeviceEventEmitter.emit(MESSAGE_CHANNEL,data);

}
const sendWarnProgress  = (code,stepV,timeV)  =>{
  data="";
  if(code==1002)
  {
    data={status:code,step:stepV}
  }
  else if(code==1003)
  {
    data={status:code,step:stepV,time:timeV}
  }
  console.log("send warn param"+JSON.stringify(data));
  DeviceEventEmitter.emit(MESSAGE_CHANNEL,data);

}

const parseTemp  = (jsonValue)  =>{
    if(jsonValue['current'] ||jsonValue['target'] )
    {
        console.log("from Appliance: current :  "+jsonValue['current'] + "target:"+jsonValue['target']);
        currentTemp = parseInt(jsonValue['current']) ;
        targetTemp  = parseInt(jsonValue['target']) ;
        //add steak
        if(currentTemp>targetTemp)
         {
           console.log("beyond ,ignore");
           reurn 
         }
        if(getNowStep()==COOK_STEP.steak_lid )
        {
          param={step: 4, progress: [{val: currentTemp,target:targetTemp}]}
          sendEvent(param)
        }
        else if(getNowStep()==COOK_STEP.steak_flip)
        {
          param={step: 5, progress: [{val: currentTemp,target:targetTemp}]}
          sendEvent(param)
        }

        //alert("recipe selected")
    }
    else if(jsonValue['st']==="preheating")
    {
        console.log("from parseTemp: preheating now animation "+jsonValue['animation'] )
        preheating_animation_now = parseInt(jsonValue['animation']);
        setNowStep(COOK_STEP.preheating)
        param = "";
        if(preheating_animation==0)
        {
          param={step:2, progress: [{val: 8}]}
          sendEvent(param)
          param={step:3, progress: [{},{val: preheating_animation_now}]} 
          sendEvent(param)
        }
        else
        {
          param={step:3, progress: [{},{val: preheating_animation_now}]} 
          sendEvent(param)
        }
        preheating_animation = preheating_animation_now ;
    }  
}
const parseWarn  = (jsonValue)  =>{
  if(jsonValue['warn']==="lid open")
    {
        console.log("from Appliance:   "+jsonValue )
        sendWarn(MESSAGE_OPEN_LID);
        nowStep = getNowStep() ;
        if(getNowStep() >=COOK_STEP.cauliflower_select)
        {
           src_timer = jsonValue['timer']
           src_targetTime = jsonValue['target']

           timer = parseInt(src_timer.replace(/^#/, ''), 16);
           targetTime = parseInt(src_targetTime.replace(/^#/, ''), 16);
           console.log("timer:" + src_timer +"->"+ timer + " "+src_targetTime+"->"+targetTime);
           if(nowStep <COOK_STEP.cauliflower_flip )
             {
               sendWarnProgress(1002,7)
              }
              else if(nowStep === COOK_STEP.cauliflower_flip)
              {
                sendWarnProgress(1002,8)
              }
              else if(nowStep === COOK_STEP.add_salsa)
              {
                sendWarnProgress(1002,9)
              }
        }
    }
    else if(jsonValue['warn']==="lid closed")
    {
        console.log("from Warn:   "+jsonValue  )
        sendWarn(MESSAGE_CLOSE_LID);

        if(getNowStep() >=COOK_STEP.cauliflower_select)
        {   

          src_timer = jsonValue['timer']
          src_targetTime = jsonValue['target']
  
          timer = parseInt(src_timer.replace(/^#/, ''), 16);
          targetTime = parseInt(src_targetTime.replace(/^#/, ''), 16);
          if(nowStep == COOK_STEP.cauliflower_ack)
          {
            //this is previous step,it may not 
              setNowStep(COOK_STEP.cauliflower_cook)
          }
          if(nowStep <COOK_STEP.cauliflower_flip )
          {
             realCountdown = targetTime - timer - 300;
             if(realCountdown<0)
             {
              realCountdown = 1;
             }
             console.log("timer:" + src_timer +"->"+ timer + " "+src_targetTime+"->"+targetTime+":real"+realCountdown);
             sendWarnProgress(1003,7,realCountdown);
          }
          else if(nowStep === COOK_STEP.cauliflower_flip)
          {
            realCountdown = targetTime - timer - 120;
            if(realCountdown<0)
            {
              realCountdown = 1 ;
            }
            console.log("timer:" + src_timer +"->"+ timer + " "+src_targetTime+"->"+targetTime+":real"+realCountdown);
            sendWarnProgress(1003,8,realCountdown)
          }
          else if(nowStep === COOK_STEP.add_salsa)
          {
            realCountdown = targetTime - timer;
            if(realCountdown < 0)
            {
              realCountdown = 1 ;
            }
            console.log("timer:" + src_timer +"->"+ timer + " "+src_targetTime+"->"+targetTime+":real"+realCountdown);
            sendWarnProgress(1003,9,realCountdown)
          }
        }
    }
  
}
const setAddSteak= ()  =>{

  if(getNowStep()==COOK_STEP.preheating)
  {
      setNowStep(COOK_STEP.steak_lid)
      // param={step: 4, progress: [{val: 8}]}
      // sendEvent(param)
      
  }
}
const parseOutput = (jsonValue)  =>{
      
    //use step
    if(jsonValue['ack']==="recipe selected")
    {
        console.log("from Appliance: "+ jsonValue['ack'])
        laststep=getNowStep();
        if(laststep==COOK_STEP.steak_select)
        {
            // setNowStep(COOK_STEP.steak_ack)
            // param={step: 1, progress: [{val: 8}]}
            // sendEvent(param)
        }
        else if(laststep==COOK_STEP.cauliflower_select)
        {
           setNowStep(COOK_STEP.cauliflower_ack)
          //  param={step: 7, progress: [{val: 8,time:180}]}
          //  sendEvent(param)
           sendWarnProgress(1003, 7,180)
           setNowStep(COOK_STEP.cauliflower_cook)
        }
        else
        {
            console.log("warn: you need send recipe first")
            //alert("warn: you need send recipe first")
        }
        //alert("recipe selected")
    }
    if(jsonValue['st']==="preheating")
    {
        console.log("from Appliance: preheating now animation "+jsonValue['animation'] )
        preheating_animation_now = parseInt(jsonValue['animation']);
        setNowStep(COOK_STEP.preheating)
        param = "";
        if(preheating_animation==0)
        {
          param={step:2, progress: [{val: 8}]}
          sendEvent(param)
          param={step:3, progress: [{},{val: preheating_animation_now}]} 
          sendEvent(param)
        }
        else
        {
          param={step:3, progress: [{},{val: preheating_animation_now}]} 
          sendEvent(param)
        }
        preheating_animation = preheating_animation_now ;
        
        //console.log("send event param 11"+JSON.stringify(param));
        //DeviceEventEmitter.emit(COOKING_STEPS_CHANNEL,param);
        //alert("recipe selected")
    }  
    else if(jsonValue['st']==="add food")
    {
       if(preheating_animation === 0)
       {
        param={step:2, progress: [{val: 8}]}
        sendEvent(param)
       }
       laststep=getNowStep();
       if(laststep!==COOK_STEP.preheating)
       {
           setNowStep(COOK_STEP.preheating)
       }
        param={step:3, progress: [{},{val: 8}]}
        sendEvent(param)
        preheating_animation = 0 
          
    }
    if(jsonValue['warn']==="lid open")
    {
        console.log("from Appliance:   "+jsonValue )
        sendWarn(MESSAGE_OPEN_LID);
        nowStep = getNowStep() ;
        if(getNowStep() >=COOK_STEP.cauliflower_select)
        {
           src_timer = jsonValue['timer']
           src_targetTime = jsonValue['target']

           timer = parseInt(src_timer.replace(/^#/, ''), 16);
           targetTime = parseInt(src_targetTime.replace(/^#/, ''), 16);
           console.log("timer:" + src_timer +"->"+ timer + " "+src_targetTime+"->"+targetTime);
          //  if(nowStep == COOK_STEP.cauliflower_ack)
          //  {
          //      setNowStep(COOK_STEP.cauliflower_cook)
          //      realCountdown = 180 - timer;
          //      param={step: 7, progress: [{val: 8,time:realCountdown}]}
          //      sendEvent(param)
          //  }
           if(nowStep <COOK_STEP.cauliflower_flip )
             {
               sendWarnProgress(1002,7)
              }
              else if(nowStep === COOK_STEP.cauliflower_flip)
              {
                sendWarnProgress(1002,8)
              }
              else if(nowStep === COOK_STEP.add_salsa)
              {
                sendWarnProgress(1002,9)
              }
        }
        // if(getNowStep()==COOK_STEP.preheating)
        // {
        //     setNowStep(COOK_STEP.steak_lid)
        //     sendWarn(MESSAGE_OPEN_LID);
        //     // param={step: 4, progress: [{val: 8}]}
        //     // sendEvent(param)
            
        //     tempTimeout = setInterval(() => {
        //       readCloudMessageTemp();
        //   }, 1000 * 5);
        // }
        //alert("recipe selected")
    }
    else if(jsonValue['warn']==="lid closed")
    {
        console.log("from Appliance:   "+jsonValue  )
        sendWarn(MESSAGE_CLOSE_LID);

        if(getNowStep() >=COOK_STEP.cauliflower_select)
        {   

          src_timer = jsonValue['timer']
          src_targetTime = jsonValue['target']
  
          timer = parseInt(src_timer.replace(/^#/, ''), 16);
          targetTime = parseInt(src_targetTime.replace(/^#/, ''), 16);
          if(nowStep == COOK_STEP.cauliflower_ack)
          {
            //this is previous step,it may not 
              setNowStep(COOK_STEP.cauliflower_cook)
          }
          if(nowStep <COOK_STEP.cauliflower_flip )
          {
             realCountdown = targetTime - timer - 300;
             if(realCountdown<0)
             {
              realCountdown = 1;
             }
             console.log("timer:" + src_timer +"->"+ timer + " "+src_targetTime+"->"+targetTime+":real"+realCountdown);
             sendWarnProgress(1003,7,realCountdown);
          }
          else if(nowStep === COOK_STEP.cauliflower_flip)
          {
            realCountdown = targetTime - timer - 120;
            if(realCountdown<0)
            {
              realCountdown = 1 ;
            }
            console.log("timer:" + src_timer +"->"+ timer + " "+src_targetTime+"->"+targetTime+":real"+realCountdown);
            sendWarnProgress(1003,8,realCountdown)
          }
          else if(nowStep === COOK_STEP.add_salsa)
          {
            realCountdown = targetTime - timer;
            if(realCountdown < 0)
            {
              realCountdown = 1 ;
            }
            console.log("timer:" + src_timer +"->"+ timer + " "+src_targetTime+"->"+targetTime+":real"+realCountdown);
            sendWarnProgress(1003,9,realCountdown)
          }
        }
    }
    else if(jsonValue['st']==="flip")
    {
        console.log("from Appliance: flip" )
        lastStep = getNowStep();
        if(lastStep==COOK_STEP.steak_lid)
        {
            if(currentTemp != targetTemp)
            {
              currentTemp = targetTemp ;
              param={step: 4, progress: [{val: currentTemp,target:targetTemp}]}
              sendEvent(param)
            }
            setNowStep(COOK_STEP.steak_flip)
            // param={step: 5, progress: [{val: currentTemp,target:targetTemp}]}
            // sendEvent(param)
            console.log("steak flip" )
        }
        else if(lastStep==COOK_STEP.cauliflower_cook)
        {
            setNowStep(COOK_STEP.cauliflower_flip)
            sendWarnProgress(1003,7,0)
            // param={step: 8, progress: [{val: 8,time:180}]}
            // sendEvent(param)
            sendWarnProgress(1003,8,180)
            console.log("cauliflower flip" )

        }
        //alert("recipe selected")
    }  
    else if(jsonValue['st']==="remove steak")
    {
        console.log("from Appliance: remove steak" )
        lastStep = getNowStep();
        if(lastStep==COOK_STEP.steak_flip)
        {
           
            if(currentTemp != targetTemp)
            {
              currentTemp = targetTemp ;
              param={step: 5, progress: [{val: currentTemp,target:targetTemp}]}
              sendEvent(param)
            }
            setNowStep(COOK_STEP.remove_steak)
            //clearInterval(tempTimeout);
            currentTemp = 0;
            param={step: 6, progress: [{val: 8}]}
            sendEvent(param)

            console.log("steak remove" )

        }
        else if(lastStep == COOK_STEP.cauliflower_flip)
        {
            setNowStep(COOK_STEP.add_salsa)
            // param={step: 9, progress: [{val: 8,time:120}]}
            // sendEvent(param)
            console.log("steak flip" )
        }
        //alert("recipe selected")
    }
    else if (jsonValue['st']==="done")
    {
        console.log("from Appliance: done" ) 

        if(getNowStep()==COOK_STEP.add_salsa )
        {
          setNowStep(COOK_STEP.done)
          sendWarnProgress(1003,9,1)
        }
    } 
    else if (jsonValue['appliance'])
    {
        console.log("appliance"+JSON.stringify(jsonValue['appliance']))
        const child = jsonValue['appliance']
        console.log("from Appliance: cauliflower time:" +child['time'] ) 
        countDown = child['time'];
        lastStep = getNowStep();
        if(getNowStep()==COOK_STEP.cauliflower_ack)
        {
            setNowStep(COOK_STEP.cauliflower_cook)
            realCountdown = countDown - 5;
            param={step: 7, progress: [{val: 8,time:realCountdown}]}
            sendEvent(param)
        }
        else if(getNowStep()==COOK_STEP.cauliflower_cook)
        {
           realCountdown = countDown - 5;
           param={step: 7, progress: [{val: 8,time:realCountdown}]}
           sendEvent(param)
        }
        else if(getNowStep()==COOK_STEP.cauliflower_flip)
        {
           realCountdown = countDown - 2;
           param={step: 8, progress: [{val: 8,time:realCountdown}]}
           sendEvent(param)
        }
        else if(getNowStep()==COOK_STEP.add_salsa)
        {
           realCountdown = countDown - 0;
           param={step: 9, progress: [{val: 8,time:realCountdown}]}
           sendEvent(param)
        }
        else{
            console.log("not in ack status :",lastStep)
            //alert("warn: you need send califlower recipe first")
        }
    }    
    else if (jsonValue['st']==="done")
    {
        console.log("from Appliance: done" ) 
        setNowStep(COOK_STEP.done)
        param={step:10, progress: [{val: 8}]}
        sendEvent(param)
    }   


}

  export default {
    setRecipe,
    setNowStep,
    //getPropertyValue,
    setPropertyValue,
    loginCloud,
    setWifi,
    switchDSN,
    setPairing,
  };
  