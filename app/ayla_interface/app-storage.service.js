import {LogBox, YellowBox} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

const MAPPING_KEY = 'mapping';
const THEME_KEY = 'theme';
const TOKEN_KEY = 'userToken';
const CURRENT_DEVICE_KEY = 'currentDevice';
const COUNTRY_KEY = 'currentCountry';
const STEP_KEY='cookStep'

export class AppStorage {
  static getMapping = fallback => {
    return AsyncStorage.getItem(MAPPING_KEY).then(mapping => {
      return mapping || fallback;
    });
  };

  static getToken = fallback => {
    return AsyncStorage.getItem(TOKEN_KEY).then(token => {
      return token || fallback;
    });
  };

  static getTokenAsync = async () => {
    const tokenStr = await AsyncStorage.getItem(TOKEN_KEY);
    const userToken = JSON.parse(tokenStr);
    return userToken;
  };

  static getCountryAsync = async () => {
    const country = await AsyncStorage.getItem(COUNTRY_KEY);
    return country;
  };

  static getDevice = fallback => {
    return AsyncStorage.getItem(CURRENT_DEVICE_KEY).then(device => {
      return device || fallback;
    });
  };

  static getTheme = fallback => {
    return AsyncStorage.getItem(THEME_KEY).then(theme => {
      return theme || fallback;
    });
  };

  static getStep = fallback => {
    return AsyncStorage.getItem(STEP_KEY).then(step => {
      return step  || fallback;
    });
  };

  static removeToken = async () => {
    return await AsyncStorage.removeItem(TOKEN_KEY);
  };

  static setMapping = mapping => {
    return AsyncStorage.setItem(MAPPING_KEY, mapping);
  };

  static setTheme = theme => {
    return AsyncStorage.setItem(THEME_KEY, theme);
  };

  static setToken = token => {
    return AsyncStorage.setItem(TOKEN_KEY, token);
  };

  static setCountry = country => {
    return AsyncStorage.setItem(COUNTRY_KEY, country);
  };

  static setDevice = device => {
    return AsyncStorage.setItem(CURRENT_DEVICE_KEY, device);
  };

  static setStep = step => {
    return AsyncStorage.setItem(STEP_KEY, step);
  };
}

/**
 * In a Bare React Native project you should use
 * https://github.com/react-native-community/async-storage
 *
 * However, Expo runs AsyncStorage exported from react-native.
 * Just to save application bundle size, we still using this one.
 */
//YellowBox.ignoreWarnings(['AsyncStorage has been extracted']);
LogBox.ignoreLogs(['AsyncStorage has been extracted'])
