import {AppStorage} from '../ayla_interface/app-storage.service';
import {Platform} from 'react-native';
import moment from 'moment';

const _URL = require('url');

/**
 * Ayla API
 * @param {*} params
 */
const aylaApiFetchAsync = async (params = {}) => {
  const headers = {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  };

  if (params.useToken === undefined || params.useToken) {
    const userToken = await AppStorage.getTokenAsync();
    headers.authorization = `auth_token ${userToken.access_token}`;
    console.log("aylaApiFetchAsync: token" + userToken.access_token)
  }

  const requestInit = {
    method: params.method,
    headers: headers,
  };
  if (params.data) {
    requestInit.body = JSON.stringify(params.data);
     console.log("aylaApiFetchAsync: body :" + requestInit.body)
  }
  
  return new Promise((resolve, reject) => {
    fetch(`${params.url}`, requestInit)
      .then(async response => {
        const status = response.status;
        console.log("aylaApiFetchAsync params ret:" + status + " url:"+params.url)
        if (status >= 200 && status < 300) {
          try {
            const content = await response.json();
            //resolve(content);
            resolve(content);
          } catch (error) {
            resolve({
              msg: '',
              status: status,
            });
          }
        } else if (status === 401) {
          const content = await response.json();
          resolve({
            msg: content,
            status: status,
          });
        } else {
          const content = await response.text();
          resolve({
            status: status,
            msg: content,
          });
        }
      })
      .catch(error => reject(error));
  });
};

const getFileData = url => {
  const params = {
    url: url,
    method: 'get',
  };
  return aylaApiFetchAsync(params);
};

const refreshData = url => {
  const params = {
    url: url,
    method: 'put',
    data: {datapoint: {fetched: true}},
  };
  return aylaApiFetchAsync(params);
};

/**
 * 刷新令牌
 * @param {*} token
 */
const refreshToken = _refreshToken => {
  const params = {
    url: `${global.aylaConfig.user_dev}/users/refresh_token`,
    method: 'post',
    data: {
      user: {
        refresh_token: _refreshToken,
      },
    },
  };
  return aylaApiFetchAsync(params);
};

/**
 * 检索指定设备上的设备属性
 * @param {*} dsn 唯一的设备序列号
 * @param {*} name 属性名称(PbInput, PbOutput, live_file)
 */
const getPropertiesByPropertyName = async (dsn, name) => {
  const params = {
    url: `${global.aylaConfig.ads_dev}/apiv1/dsns/${dsn}/properties/${name}`,
    method: 'GET',
  };
  console.log("getPropertiesByPropertyName begin :" + params.url)
  return aylaApiFetchAsync(params);
};

const getPbOutputProperties = async dsn => {
  const data = {
    url: `${global.aylaConfig.ads_dev}/apiv1/dsns/${dsn}/properties/PbOutput`,
    useToken: true,
    method: 'get',
  };
  return aylaApiFetchAsync(data);
};

const sendPbInput = async (dsn, value,propertyName) => {
  const data = {
    url: `${
      global.aylaConfig.ads_dev
    }/apiv1/dsns/${dsn}/properties/${propertyName}/datapoints`,
    useToken: true,
    method: 'post',
    data: {
      datapoint: {
        value: value,
        metadata: {
          key1: `${new Date().getTime()}`,
          timestamp: `${new Date().getTime()}`,
        },
      },
    },
  };
  return aylaApiFetchAsync(data);
};

const getDatapoints = async (params = {}) => {
  const url = _URL.format({
    protocol: global.aylaConfig.protocol,
    hostname: global.aylaConfig.domain_dev,
    pathname: `/apiv1/properties/${params.propertyId}/datapoints`,
    query: params,
  });
  const data = {
    url: url.toString(),
    useToken: true,
    method: 'get',
  };
  // console.log(data);
  return aylaApiFetchAsync(data);
};

const getDevices = async params => {
  const data = {
    url: `${global.aylaConfig.ads_dev}/apiv1/devices`,
    useToken: true,
    method: 'get',
  };
  return aylaApiFetchAsync(data);
};

const deleteDevice = async params => {
  const data = {
    url: `${global.aylaConfig.ads_dev}/apiv1/devices/${params.devId}`,
    useToken: true,
    method: 'delete',
  };
  return aylaApiFetchAsync(data);
};

const getPropertiesById = async id => {
  const params = {
    url: `${global.aylaConfig.ads_dev}/apiv1/properties/${id}`,
    method: 'GET',
  };
  return aylaApiFetchAsync(params);
};

const readBufferAsync = async url => {
  // const userToken = await AppStorage.getTokenAsync();
  return new Promise((resolve, reject) => {
    fetch(url)
      .then(async response => response.blob())
      .then(content => {
        // eslint-disable-next-line no-undef
        const reader = new FileReader();
        reader.onload = e => {
          const data = e.target.result;
          // eslint-disable-next-line no-undef
          const buffer = Buffer.from(data.split('base64,')[1], 'base64');
          resolve(buffer);
        };
        reader.onerror = reject;
        reader.readAsDataURL(content);
      })
      .catch(error => reject(error));
  });
};

/**
 * 检查注册获选者是否上线
 * @param {*} dsn
 * @param {*} setup_token
 */
const checkDeviceIsOnline = async (dsn, setup_token) => {
  const userToken = await AppStorage.getTokenAsync();
  return new Promise((resolve, reject) => {
    fetch(
      `${
        global.aylaConfig.ads_dev
      }/apiv1/devices/connected.json?dsn=${dsn}&setup_token=${setup_token}`,
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          authorization: `auth_token ${userToken.access_token}`,
          'Content-Type': 'application/json',
        },
      },
    )
      .then(async response => {
        if (response.status === 200) {
          const content = await response.json();
          resolve(content);
        } else {
          const content = await response.text();
          reject({status: response.status, content: content});
        }
      })
      .catch(err => reject(err));
  });
};

const getDeviceRegistrationCandidates = async dsn => {
  const userToken = await AppStorage.getTokenAsync();
  return new Promise((resolve, reject) => {
    fetch(
      `${
        global.aylaConfig.ads_dev
      }/apiv1/devices/register?regtype=AP-Mode&dsn=${dsn}&time=5`,
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          authorization: `auth_token ${userToken.access_token}`,
          'Content-Type': 'application/json',
        },
      },
    ).then(async response => {
      console.warn('RegistrationCandidates', response.status);
      if (response.status === 200) {
        const content = await response.json();
        resolve(content);
      } else {
        const error = await response.text();
        reject({
          status: response.status,
          msg: error,
        });
      }
    });
  });
};

const registerDevice = async (dsn, setup_token, ip) => {
  const userToken = await AppStorage.getTokenAsync();
  const data = {
    device: {
      dsn: dsn,
      regtoken: '',
      setup_token: setup_token,
      reg: '',
      ip: ip,
      lat: 0,
      lng: 0,
    },
  };
  return new Promise((resolve, reject) => {
    fetch(`${global.aylaConfig.ads_dev}/apiv1/devices`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        authorization: `auth_token ${userToken.access_token}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    }).then(async response => {
      if (response.status === 404 || response.status === 424) {
        resolve({
          status: response.status,
        });
      } else if (response.status === 201) {
        const content = await response.json();
        resolve(content);
      } else {
        const error = await response.text();
        reject({
          status: response.status,
          msg: error,
        });
      }
    });
  });
};

const getDeviceProperties = async dsn => {
  data = {
    url: `${global.aylaConfig.ads_dev}/apiv1/dsns/${dsn}/properties`,
    useToken: true,
    method: 'GET',
  };
  return aylaApiFetchAsync(data);
};

const getDeviceInfoByDevId = async devId => {
  const userToken = await AppStorage.getTokenAsync();
  console.warn(userToken);
  return new Promise((resolve, reject) => {
    fetch(`${global.aylaConfig.ads_dev}/apiv1/devices/${devId}`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        authorization: `auth_token ${userToken.access_token}`,
        'Content-Type': 'application/json',
      },
    })
      .then(response => response.json())
      .then(content => resolve(content))
      .catch(err => reject(err));
  });
};

/**
 * 用户注册
 * @param {*} email
 * @param {*} password
 * @param {*} firstname
 * @param {*} lastname
 */
const signUp = (email, password, firstname, lastname) => {
  const data = {
    user: {
      email: email,
      password: password,
      firstname: firstname,
      lastname: lastname,
      application: {
        app_id: global.aylaConfig.app_id,
        app_secret: global.aylaConfig.app_secret,
      },
      country: '必填项',
      city: '必填项',
      street: '',
      zip: '',
      phone_country_code: '',
      phone: '',
    },
  };
  return new Promise((resolve, reject) => {
    fetch(`${global.aylaConfig.domain_name}/users`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    })
      .then(response => response.json())
      .then(content => resolve(content))
      .catch(err => reject(err));
  });
};

/**
 * 用户登陆
 * @param {*} email
 * @param {*} password
 */
const signIn = (email, password) => {
  const data = {
    user: {
      email: email,
      password: password,
      application: {
        app_id: global.aylaConfig.app_id,
        app_secret: global.aylaConfig.app_secret,
      },
      body: JSON.stringify(data),
    },
  };
  console.log(JSON.stringify(data));
  return new Promise((resolve, reject) => {
    fetch(`${global.aylaConfig.domain_name}/users/sign_in`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    })
      .then(response => response.json())
      .then(content => resolve(content))
      .catch(error => reject(error));
  });
};

const getDeviceInfo = () => lanRequest('status.json'); // 获取设备信息
const getWifiStatus = () => lanRequest('wifi_status.json'); // 获取Wi-Fi信息
const getWifiScanResult = () => lanRequest('wifi_scan_results.json');
const wifiScan = () => lanRequest('wifi_scan.json', 'POST');
const connectWifi = param => {
  const ssid = encodeURIComponent(param.ssid);
  const key = encodeURIComponent(param.key);
  return lanRequest(
    `wifi_connect.json?ssid=${ssid}&key=${key}&setup_token=${
      param.setup_token
    }`,
    'POST',
  );
};

const lanRequest = (uri, method = 'GET') => {
  const url = `${global.aylaConfig.lan_ap_domain}/${uri}`;
  if (Platform.OS === 'android') {
    return xmlHttpRequestAsync(url, method);
  } else {
    return fetchAsync(url, method);
  }
};

const xmlHttpRequestAsync = (url, method, param = {}) => {
  return new Promise((resolve, reject) => {
    const request = new XMLHttpRequest();
    request.open(method, url);
    request.setRequestHeader('Accept', 'application/json');
    request.setRequestHeader('Content-Type', 'application/json');
    request.onload = () => {
      const status = request.status;
      // console.log(request.response, status);
      if (status === 204) {
        resolve({
          status: 204,
        });
      } else if (status >= 200 && status < 300) {
        try {
          const content = JSON.parse(request.response);
          resolve(content);
        } catch (error) {
          reject({
            error: error,
            status: status,
          });
        }
      } else {
        reject({
          status: status,
          msg: request.response,
        });
      }
    };
    request.onerror = () => {
      reject({
        status: request.status,
        msg: request.response,
      });
    };
    request.send(JSON.stringify(param));
  });
};

const fetchAsync = (url, method, param = {}) => {
  return new Promise((resolve, reject) => {
    fetch(url, {
      method: method,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then(async response => {
        switch (response.status) {
          case 200:
            return response.json();
          case 404:
            reject({});
            break;
          case 204:
            resolve({status: 204});
            break;
          default:
            console.warn(response.status);
            const content = await response.text();
            console.warn(content);
        }
      })
      .then(content => resolve(content))
      .catch(error => reject(error));
  });
};

const getPropertyValue = async (dsn, name) => {
  let result = await getPropertiesByPropertyName(dsn, name);
  if (!result || !result.property || !result.property.value) {
    return;
  }
  result = await getPropertiesById(result.property.key);
  const created_at_since_date = moment()
    .utc()
    .subtract(60 * 0.5, 'seconds')
    .format();
  result = await getDatapoints({
    propertyId: result.property.key,
    per_page: 10,
    limit: 3,
    is_forward_page: true,
    paginated: false,
    'filter[created_at_since_date]': created_at_since_date,
  });
  if (!result || result.length === 0) {
    return;
  }
  result = result[result.length - 1];
  if (!result || !result.datapoint || !result.datapoint.value) {
    return;
  }
  return result.datapoint.value;
};

export default {
  getPropertyValue,
  signIn,
  signUp,
  refreshToken,
  getDeviceInfo,
  getWifiStatus,
  getWifiScanResult,
  wifiScan,
  connectWifi,
  getDeviceInfoByDevId,
  getDeviceProperties,
  registerDevice,
  getDeviceRegistrationCandidates,
  checkDeviceIsOnline,
  getDevices,
  deleteDevice,
  getPbOutputProperties,
  getDatapoints,
  sendPbInput,
  getPropertiesByPropertyName,
  readBufferAsync,
  getFileData,
  getPropertiesById,
  refreshData,
};
