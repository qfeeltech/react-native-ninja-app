import  {DeviceEventEmitter} from 'react-native';
const MEESSAGE_IOT_DEBUG="iotDebug"

var inputDict={
    "rib eye":"C303070601RIB EYE13100053",
    "cauliflower":"C303070601CAULIFR010800DD"
}
var outputDict = {
    "C303070609.0048.007E.A2":`{"name":"test"}`,
    "C303070602.D5":`{"ack": "recipe selected"}`,
    "C303070603.03.D9":`{"st": "preheating", "animation": "1"}`,
    "C30307060B.D9":`{"st": "preheating", "animation": "8"}`,
    "C303070604.00.0000.0258.2F":`{"warn":"lid open"}`,
    "C303070604.01.0000.0258.30":`{"warn":"lid close"}`,
    "C303070607.A2":`{"st":"flip"}`,
    "C30307060A.A2":`{"st":"remove steak"}`,
    "C303070601CAULIFR010800DD":`{
        "appliance": {
            "status": "cauliflower cooking ",
            "time": 240,
            "af_temp": 120,
            "gr_temp": 120
        }
    }`,
  }


const translatePbInput = (input) => {

    const jsonValue =  JSON.parse(input);

    if(jsonValue['recipe']['name'] )
    {
      console.log("origin:" + input + " target:"+inputDict[jsonValue['recipe']['name']]);
      return inputDict[jsonValue['recipe']['name']]
    }
    else{
        console.log("name error")
    }
    
}
const translatePbOutput = (input) => {

    //only for test
    // if(input=="C303070609.0048.007E.A2")
    // {
    //     return translateTemp(input)
    // }
    console.log("origin:" + input + " target:"+outputDict[input]);
    return outputDict[input];
}

const translateTemp = (input) => {
    //C303070609.0048.007E.A2
    temp = input
    arr=temp.split("."); 
    current=0;
    target=0;
    json_value=""
    if(arr.length===4)
    {
      cur=arr[1];
      tar=arr[2];
      current = parseInt(cur.replace(/^#/, ''), 16);
      target = parseInt(tar.replace(/^#/, ''), 16);
    }
    else
    {
       console.log("error translateTemp size error"+arr.length)
    }
    json_value= `{"current": ${current},"target": ${target}}` 
    console.log("origin:" + input + " target:"+json_value);
    //DeviceEventEmitter.emit(MEESSAGE_IOT_DEBUG,{'name':'ProbeTemp','src':input,"dst":json_value});
    return json_value;
}

export default {
    translatePbInput,
    translatePbOutput,
    translateTemp,
  };