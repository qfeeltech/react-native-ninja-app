/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react/display-name */
import * as React from "react"
import { View, ViewStyle, Image } from "react-native"
import { observer } from "mobx-react-lite"
import { color } from "../../theme"
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'

const Tab = createBottomTabNavigator()

export interface BottomNavigationProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: ViewStyle
  renderScene: any
}

/**
 * Describe your component here
 */
export const BottomNavigation = observer(function BottomNavigation(props: BottomNavigationProps) {
  const { renderScene } = props

  const [sceneMap] = React.useState(renderScene)

  const [routes] = React.useState([
    {
      key: 'home',
      icon: require("./icons/home.png"),
      focused: require("./icons/home_focused.png"),
    },
    {
      key: 'user',
      icon: require("./icons/user.png"),
      focused: require("./icons/user_focused.png"),
    },
    {
      key: 'mark',
      icon: require("./icons/mark.png"),
      focused: require("./icons/mark_focused.png"),
    },
    {
      key: 'search',
      icon: require("./icons/search.png"),
      focused: require("./icons/search_focused.png"),
    },
  ])

  return (
    <Tab.Navigator
      backBehavior="none"
      tabBarOptions={{
        showLabel: false,
        style: {
          backgroundColor: color.palette.white,
          elevation: 0,
          height: 66
        }
      }}
      initialRouteName="Home">
      {routes.map((item, index) => (
        <Tab.Screen
          key={index}
          name={item.key}
          component={sceneMap[item.key]}
          options={{
            tabBarIcon: ({ focused }) => (
              <View style={{ marginTop: 20 }}>
                <Image
                  source={focused ? item.focused : item.icon}
                  resizeMode="contain"
                  style={{
                    width: 18,
                    height: 18,
                  }}
                />
              </View>
            ),
          }}
        />
      ))}
    </Tab.Navigator>
  )
})
