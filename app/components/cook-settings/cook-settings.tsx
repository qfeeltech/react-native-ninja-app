import * as React from "react"
import { TextStyle, View, ViewStyle, Dimensions, Image, ImageSourcePropType, TouchableOpacity } from "react-native"
import { observer } from "mobx-react-lite"
import { color, typography, style } from "../../theme"
import { Slider } from "react-native-elements"
import { Text, Button } from "../"

const windowWidth = Dimensions.get("window").width
const windowHeight = Dimensions.get("window").height
const ICON_SIZE = 50
const CONTAINER: ViewStyle = {
  backgroundColor: color.palette.white,
  borderBottomLeftRadius: 20,
  borderBottomRightRadius: 20,
}
const TRACK_STYLE: ViewStyle = { height: 8, borderRadius: 20 }
const THUMB_STYLE: ViewStyle = { height: 15, width: 15, backgroundColor: color.palette.lightGrey20 }
const SLIDER_STYLE: ViewStyle = {
  shadowOpacity: 1,
  shadowRadius: 3,
  shadowOffset: { width: 0, height: 3 },
  shadowColor: color.palette.shadowColor30
  // ...style.SHADOW_STYLE,
}
const SETTINGS_CONTAINER: ViewStyle = { marginHorizontal: 44, marginTop: 21 }
const ITEM_CONTAINER: ViewStyle = { marginTop: 20 }
const RANGE_CONTAINER: ViewStyle = { flex: 1, alignItems: "stretch", justifyContent: "center", marginTop: 20 }
const RANGE_DESC_CONTAINER: ViewStyle = {
  flexDirection: "row",
  justifyContent: "space-between",
  marginBottom: -10,
}
const TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 16,
  color: color.palette.titleColor10
}
const TEXT_BOLD: TextStyle = {
  ...TEXT,
  fontFamily: "Gotham-Bold",
  color: color.palette.titleColor20
}
const RANGE_VALUE_CONTAINER: ViewStyle = {
  flexDirection: "row",
  justifyContent: "space-between",
  marginLeft: 5,
  marginTop: -8
}
const RANGE_VALUE = [1, 2, 3, 4, 5, 6, 7, 8, 9]
const NUMBER = {
  ...TEXT,
  fontSize: 10,
  color: color.palette.lightGrey100,
}
const THICKNESS_CONTAINER: ViewStyle = {
  flexDirection: "row",
  alignItems: "center",
  justifyContent: "space-between",
  marginTop: 10,
  // borderWidth: 0.3,
}
const THICKNESS: ViewStyle = { flexDirection: "row" }
const TEXT_VALUE = { ...TEXT, marginLeft: 45 }
const TEXT_UNIT = { ...TEXT, marginLeft: 14 }
const ICON_BUTTON_CONTAINER: ViewStyle = {
  flexDirection: "row",
  alignItems: "center",
}
const ICON_BUTTON_STYLE = {
  width: ICON_SIZE,
  height: ICON_SIZE,
  // borderWidth: 0.3,
  // marginLeft: 20,
}
const BUTTON_CONTAINER: ViewStyle = { marginTop: 25, marginBottom: 40, alignItems: "center" }
const BUTTON: ViewStyle = {
  ...style.SHADOW_STYLE,
  width: windowWidth / 1.8382,
  height: 48,
}
const BUTTON_LABEL = { ...TEXT, fontSize: 19, color: color.palette.white }
const INGREDIENTS_CONTAINER: ViewStyle = { marginBottom: 40 }
const TEXT_STEPS = { ...TEXT, marginVertical: 2 }
const TITLE = { ...TEXT_BOLD, fontSize: 21, color: color.palette.labelColor10, letterSpacing: 0, lineHeight: 21 }
const TITLE_CONTAINER: ViewStyle = {
  justifyContent: "center",
  alignItems: "center",
  height: 18,
  // borderWidth: 1
}
const IMAGE_CONTAINER: ViewStyle = { alignItems: "center", justifyContent: "center" }
const GRAYSCALE_STYLE: ViewStyle = {
  opacity: 0.3,
}

export interface CookSettingsProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: ViewStyle
  contentStyle?: ViewStyle
  itemStyle?: ViewStyle
  imageStyle?: ViewStyle
  source: ImageSourcePropType
  sliderValue?: number
  grayscale?: boolean
  onChangeSliderValue?(value: number): void
  onPress?(): void
  ingredients?: string[]
  disabled?: boolean
  title?: string
  imageWidth?: number
  imageHeight?: number
  buttonLabel: string
}

/**
 * Describe your component here
 */
export const CookSettings = observer(function CookSettings(props: CookSettingsProps) {
  const {
    title,
    onPress,
    onChangeSliderValue,
    disabled,
    grayscale,
    contentStyle,
    itemStyle,
    imageStyle,
    style,
    source,
    ingredients,
    imageWidth,
    imageHeight,
    buttonLabel,
    sliderValue
  } = props

  const imgWidth = imageWidth !== undefined ? imageWidth : windowWidth
  const imgHeight = imageHeight !== undefined ? imageHeight : windowHeight / 2.88

  const thicknessOptions = [1, 1.5, 2]
  const [thicknessIndex, setThicknessIndex] = React.useState(0)

  const onChangeThickness = (flag) => {
    if (flag === "-") {
      setThicknessIndex(thicknessIndex - 1)
    } else if (flag === "+") {
      setThicknessIndex(thicknessIndex + 1)
    }
  }

  return (
    <View style={[CONTAINER, style, grayscale ? GRAYSCALE_STYLE : {}]}>
      <View>
        {title ? <View style={TITLE_CONTAINER}>
          <Text style={TITLE}>{title}</Text>
        </View> : null}
        <View style={[IMAGE_CONTAINER, imageStyle]}>
          <Image style={{ height: imgHeight, width: imgWidth }} source={source} />
        </View>
        <View style={[SETTINGS_CONTAINER, contentStyle]}>
          <Text style={[TEXT, {}]}>Cook time: 40 minutes</Text>
          <View style={[ITEM_CONTAINER, itemStyle]}>
            <Text style={TEXT_BOLD}>Doneness</Text>
            <View style={RANGE_CONTAINER} >
              <View style={RANGE_DESC_CONTAINER}>
                <Text style={TEXT}>Rare</Text>
                <Text style={TEXT}>Medium</Text>
                <Text style={TEXT}>Well</Text>
              </View>
              <Slider
                maximumValue={9}
                minimumValue={1}
                step={1}
                value={sliderValue}
                onValueChange={onChangeSliderValue}
                maximumTrackTintColor={color.palette.lightGrey10}
                minimumTrackTintColor={color.palette.transparent}
                trackStyle={TRACK_STYLE}
                thumbStyle={THUMB_STYLE}
                style={SLIDER_STYLE}
              />
            </View>
            <View style={RANGE_VALUE_CONTAINER}>
              {RANGE_VALUE.map((value, index) => (
                <Text style={NUMBER} key={index}>
                  {value}
                </Text>
              ))}
            </View>
            <View style={THICKNESS_CONTAINER}>
              <View style={THICKNESS}>
                <Text style={TEXT_BOLD}>Thickness</Text>
                <View style={THICKNESS}>
                  <Text style={TEXT_VALUE}>{thicknessOptions[thicknessIndex]}</Text>
                  <Text style={TEXT_UNIT}>in</Text>
                </View>
              </View>
              <View style={ICON_BUTTON_CONTAINER}>
                <TouchableOpacity
                  disabled={thicknessIndex === 0}
                  onPress={() => onChangeThickness("-")}>
                  <Image style={ICON_BUTTON_STYLE} source={require("./icons/minus.png")} />
                </TouchableOpacity>
                <TouchableOpacity
                  disabled={thicknessIndex === (thicknessOptions.length - 1)}
                  onPress={() => onChangeThickness("+")}>
                  <Image style={ICON_BUTTON_STYLE} source={require("./icons/plus.png")} />
                </TouchableOpacity>
              </View>
            </View>
            <View style={BUTTON_CONTAINER}>
              <Button style={BUTTON} onPress={onPress} disabled={disabled}>
                <Text style={BUTTON_LABEL}>{buttonLabel}</Text>
              </Button>
            </View>
            {ingredients ? <View style={INGREDIENTS_CONTAINER}>
              <Text style={TEXT_BOLD}>Ingredients:</Text>
              <>
                {ingredients.map((value, index) => (
                  <Text style={TEXT_STEPS} key={index}>
                    {value}
                  </Text>
                ))}
              </>
            </View> : null}
          </View>
        </View>
      </View>
    </View>
  )
})
