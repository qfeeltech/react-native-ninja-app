/* eslint-disable react-native/no-inline-styles */
import * as React from "react"
import {
  TextStyle,
  View,
  ViewStyle,
  Image,
  ImageSourcePropType,
  TouchableOpacity,
  Dimensions,
  ImageStyle,
} from "react-native"
import { observer } from "mobx-react-lite"
import { Text } from ".."
import { color, typography } from "../../theme"
import { Modal as ModalComponent, Colors } from "react-native-paper"

const windowWidth = Dimensions.get("window").width
const CONTAINER: ViewStyle = { margin: 10, backgroundColor: Colors.white, borderRadius: 20 }

const TITLE: TextStyle = {
  fontFamily: typography.bold,
  fontSize: 16,
  color: color.palette.titleColor,
}

const DESC: TextStyle = {
  // fontFamily: "Gotham-Regular",
  fontSize: 16,
  color: color.palette.lightGrey20,
}

const IMAGE_STYLE: ImageStyle = {
  width: windowWidth / 1.25,
  height: windowWidth / 1.64,
  resizeMode: "stretch",
}

export interface ModalProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: ViewStyle
  visible: boolean
  source: ImageSourcePropType
  title: string
  desc: string,
  hideCloseButton?: boolean
  onDismiss?(): void
}

/**
 * Describe your component here
 */
export const Modal = observer(function Modal(props: ModalProps) {
  const { source, visible, title, desc, onDismiss, hideCloseButton } = props

  return (
    <ModalComponent visible={visible} onDismiss={onDismiss} contentContainerStyle={CONTAINER}>
      <View style={{ margin: 25 }}>
        <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
          <Text style={TITLE}>{title}</Text>
          {!hideCloseButton ? <TouchableOpacity onPress={onDismiss}>
            <Image source={require("./icons/closed.png")} />
          </TouchableOpacity> : null}
        </View>
        <View style={{ marginVertical: 25 }}>
          <Text style={DESC}>{desc}</Text>
        </View>
        <View style={{ justifyContent: "center", alignItems: "center" }}>
          <Image style={[IMAGE_STYLE, {}]} source={source} />
        </View>
      </View>
    </ModalComponent >
  )
})
