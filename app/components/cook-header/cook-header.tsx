/* eslint-disable react-native/no-inline-styles */
import * as React from "react"
import { TextStyle, View, ViewStyle, TouchableOpacity, Image, Dimensions } from "react-native"
import { observer } from "mobx-react-lite"
import { color, typography } from "../../theme"
import { Text } from "../"

const windowWidth = Dimensions.get("window").width
// const windowHeight = Dimensions.get("window").height

const CONTAINER: ViewStyle = {
  alignItems: "center",
  justifyContent: "space-evenly",
  backgroundColor: color.palette.white,
  height: 78,
  flexDirection: "row",
}

const TEXT: TextStyle = {
  width: windowWidth * 0.65,
  fontSize: 16,
  fontFamily: typography.primary,
  textAlign: "center",
  color: color.palette.titleColor,
}

const NAV_ICON_SIZE = 25

export interface CookHeaderProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: ViewStyle
  titleStyle?: TextStyle,
  iconSize?: number
  title: string

  /**
   * What happens when you press the left icon
   */
  onLeftPress?(): void
}

/**
 * Describe your component here
 */
export const CookHeader = observer(function CookHeader(props: CookHeaderProps) {
  const { style, titleStyle, title, onLeftPress } = props

  return (
    <View style={[CONTAINER, style]}>
      <View style={[{}, onLeftPress ? {} : { width: NAV_ICON_SIZE, height: NAV_ICON_SIZE }]}>
        {
          onLeftPress ? (
            <TouchableOpacity onPress={onLeftPress}>
              <Image style={{ width: NAV_ICON_SIZE, height: NAV_ICON_SIZE }} source={require("./icons/arrow-back.png")} />
            </TouchableOpacity>
          )
            : null
        }
      </View>
      <View style={{ height: 52, alignItems: "center", justifyContent: "center" }}>
        <Text style={[TEXT, titleStyle, { borderColor: color.error }]} numberOfLines={2}>
          {title}
        </Text>
      </View>
      <View style={{ width: NAV_ICON_SIZE, height: NAV_ICON_SIZE }}></View>
    </View>
  )
})
