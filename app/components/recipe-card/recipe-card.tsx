/* eslint-disable react-native/no-inline-styles */
import * as React from "react"
import { TextStyle, View, ViewStyle, Dimensions, TouchableOpacity, Image, ImageSourcePropType } from "react-native"
import { observer } from "mobx-react-lite"
import { color, typography } from "../../theme"
import { Text } from "../"
import CardView from "react-native-cardview"

const windowWidth = Dimensions.get("window").width
const windowHeight = Dimensions.get("window").height

const CONTAINER: ViewStyle = {
  width: windowWidth * 0.9,
  height: windowWidth / 3.5,
  backgroundColor: color.palette.white,
  marginHorizontal: 17,
  marginBottom: 20,
  justifyContent: "center",
  alignItems: "center",
  shadowRadius: 8,
  shadowOffset: { width: 0, height: 8 },
  shadowColor: color.palette.shadowColor10
}
const IMAGE_SIZE = windowWidth / 4.12
const CARD_CONTAINER: ViewStyle = {
  flexDirection: "row",
  // borderWidth: 1,
  // width: windowWidth - 17 * 2 - 20 * 2
}
const MARK_CONTAINER: ViewStyle = { marginTop: 20 }
const DESC_CONTAINER: ViewStyle = {
  width: windowWidth / 2.45,
  // borderWidth: 1,
  marginHorizontal: 20,
  marginTop: 20
}
const TITLE: TextStyle = {
  fontFamily: typography.bold,
  fontSize: 12,
  color: color.palette.titleColor
}
const LABEL: TextStyle = {
  marginTop: 11,
  color: color.palette.lightGrey20,
  fontSize: 12,
  fontFamily: typography.primary
}

export interface RecipeCardProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: ViewStyle
  source: ImageSourcePropType
  title: string
  cookTime: string
  onPress?(): void
}

/**
 * Describe your component here
 */
export const RecipeCard = observer(function RecipeCard(props: RecipeCardProps) {
  const {
    style,
    onPress,
    title,
    cookTime,
    source,
  } = props

  return (
    <CardView
      cardElevation={2}
      cardMaxElevation={2}
      cornerRadius={20}
      style={[CONTAINER, style]}
    >
      <View style={CARD_CONTAINER}>
        <TouchableOpacity style={{ flexDirection: "row" }} onPress={onPress}>
          <Image
            style={{ width: IMAGE_SIZE, height: IMAGE_SIZE }}
            source={source}
          />
          <View style={DESC_CONTAINER}>
            <Text style={TITLE}>{title}</Text>
            <Text style={LABEL}>Cook time: {cookTime}</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity>
          <View style={MARK_CONTAINER}>
            <Image source={require("./icons/mark.png")} />
          </View>
        </TouchableOpacity>
      </View>
    </CardView>
  )
})
