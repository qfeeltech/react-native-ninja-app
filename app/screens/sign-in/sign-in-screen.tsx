/* eslint-disable react-native/no-inline-styles */
import React from "react"
import { observer } from "mobx-react-lite"
import { ViewStyle, ImageBackground, View, Image, Dimensions } from "react-native"
import { Screen, Button, Text } from "../../components"
import { useNavigation } from "@react-navigation/native"
// import { useStores } from "../../models"
import { color, style } from "../../theme"
import cloudOperation from "../../ayla_interface/cloudOperation.js"
import { load } from "../../utils/storage"

const windowWidth = Dimensions.get("window").width
const windowHeight = Dimensions.get("window").height

const BUTTON_WIDTH = windowWidth - (44 * 2)

const ROOT: ViewStyle = {
  backgroundColor: color.palette.white,
  flex: 1,
}

export const SignInScreen = observer(function SignInScreen() {
  // Pull in one of our MST stores
  // const { someStore, anotherStore } = useStores()

  // Pull in navigation via hook
  const navigation = useNavigation()

  return (
    <Screen style={ROOT} unsafe={true}>
      <ImageBackground style={{ flex: 1 }} source={require("./bg.png")}>
        <View
          style={{
            alignItems: "flex-start",
            justifyContent: "flex-start",
            marginTop: 28,
            marginLeft: -80,
          }}
        >
          <Image
            style={{ width: windowWidth / 1.875, height: windowWidth / 1.875 }}
            source={require("./sighin_beefdinner1.png")}
          />
        </View>
        <View style={{ alignItems: "center" }}>
          <View style={{ marginBottom: 60, alignItems: "center" }}>
            <Image
              style={{ width: windowWidth / 2.67, height: windowHeight / 9.12 }}
              source={require("./logo.png")}
            />
          </View>
          <Button
            onPress={() => {
              const loadFunc = async () => {
                const value = await load("SWITCH_IOT")
                if (value && value.switchIot) {
                  cloudOperation.loginCloud()
                }
              }
              loadFunc()
              navigation.navigate("setup")
            }}
            style={{
              ...style.SHADOW_STYLE,
              width: BUTTON_WIDTH,
              height: 48,
              marginBottom: 20,
            }}>
            <Text style={{ fontFamily: "Gotham-Book", fontSize: 19 }}>New Device</Text>
          </Button>
          <Button
            // disabled={true}
            onPress={() => console.log("Create Account")}
            style={{
              ...style.SHADOW_STYLE,
              width: BUTTON_WIDTH,
              height: 48,
              marginBottom: 20,
            }}>
            <Text style={{ fontFamily: "Gotham-Book", fontSize: 19 }}>Create Account</Text>
          </Button>
          <Button
            disabled={true}
            onPress={() => console.log("")}
            style={{
              ...style.SHADOW_STYLE,
              width: BUTTON_WIDTH,
              height: 48,
              marginBottom: 20,
              backgroundColor: color.palette.white,
              borderWidth: 1,
              borderColor: color.palette.buttonColor
            }}>
            <Text style={{ fontFamily: "Gotham-Book", fontSize: 19, color: color.palette.green }}>Explore</Text>
          </Button>
        </View>
        <View
          style={{
            alignItems: "flex-end",
            justifyContent: "flex-end",
            marginRight: -80,
          }}
        >
          <Image
            style={{ width: windowWidth / 1.875, height: windowWidth / 1.875 }}
            source={require("./sighin_beefdinner2.png")}
          />
        </View>
        <View style={{ alignItems: "center", justifyContent: "center", zIndex: 1, marginTop: -43, height: 22 }}>
          <Text style={{
            color: color.palette.labelColor,
            fontWeight: "400",
            fontSize: 12
          }}>Terms and conditions</Text>
        </View>
      </ImageBackground>
    </Screen>
  )
})
