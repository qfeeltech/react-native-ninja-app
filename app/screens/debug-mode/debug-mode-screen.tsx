/* eslint-disable react/display-name */
/* eslint-disable react-native/no-inline-styles */
import React, { Fragment } from "react"
import { observer } from "mobx-react-lite"
import {
  ViewStyle,
  View,
  Dimensions,
  ScrollView,
  DeviceEventEmitter
} from "react-native"
import { Screen, Text } from "../../components"
// import { useNavigation } from "@react-navigation/native"
// import { useStores } from "../../models"
import { color } from "../../theme"
import { Switch } from "react-native-paper"
import { save, load } from "../../utils/storage"
import cloudOperation from "../../ayla_interface/cloudOperation"
import DropDownPicker from 'react-native-dropdown-picker'
import Icon from 'react-native-vector-icons/Feather'

const windowWidth = Dimensions.get("window").width
const windowHeight = Dimensions.get("window").height

const dsnItems = [
  {
    label: 'AC000W017943876',
    value: 'AC000W017943876',
    icon: () => <Icon name="flag" size={18} color="#900" />
  },
  {
    label: 'AC000W018141402',
    value: 'AC000W018141402',
    icon: () => <Icon name="flag" size={18} color="#900" />
  },
  {
    label: 'AC000W017943877',
    value: 'AC000W017943877',
    icon: () => <Icon name="flag" size={18} color="#900" />
  },
  {
    label: 'AC000W017943878',
    value: 'AC000W017943878',
    icon: () => <Icon name="flag" size={18} color="#900" />
  },
]

const ROOT: ViewStyle = {
  backgroundColor: color.palette.white,
  flex: 1,
}

export const DebugModeScreen = observer(function DebugModeScreen() {
  const [isSwitchOn, setIsSwitchOn] = React.useState(false)
  const [isSwitchLog, setIsSwitchLog] = React.useState(false)
  const [pbInput, setPbInput] = React.useState({ src: "", dst: "" })
  const [pbOutput, setPbOutput] = React.useState({ src: "", dst: "" })
  const [probeTemp, setProbeTemp] = React.useState({ src: "", dst: "" })
  const [warn, setWarn] = React.useState({ src: "", dst: "" })
  const [dsn, setDsn] = React.useState("")

  const onSelectDsnChange = (value, index) => {
    if (value !== dsn) {
      console.log(value, index)
      setDsn(value)
      const saveDsnFunc = async () => {
        await save("CURRENT_DSN", { dsn: value })
      }
      saveDsnFunc()
      cloudOperation.switchDSN(value)
    }
  }

  const onToggleSwitchLogBox = () => {
    const value = !isSwitchLog
    setIsSwitchLog(value)

    const saveFunc = async () => {
      console.log(`setIsSwitchLog: ${isSwitchLog}`)
      await save("SWITCH_LOG_BOX", { switchLogBox: value })
    }

    saveFunc()
  }

  const onToggleSwitch = () => {
    const value = !isSwitchOn
    setIsSwitchOn(value)

    const saveFunc = async () => {
      console.log(`isSwitchOn: ${isSwitchOn}`)
      await save("SWITCH_IOT", { switchIot: value })
    }
    saveFunc()
    if (value === true) {
      console.log("need login" + value)
      cloudOperation.loginCloud()
    }
  }

  // Pull in one of our MST stores
  // const { someStore, anotherStore } = useStores()

  React.useEffect(() => {
    const loadFunc = async () => {
      const value = await load("SWITCH_IOT")
      console.log(`SWITCH_IOT`, JSON.stringify(value))
      if (value && value.switchIot) {
        setIsSwitchOn(value.switchIot)
      }

      const logBoxValue = await load("SWITCH_LOG_BOX")
      console.log(`logBoxValue`, JSON.stringify(logBoxValue))
      if (logBoxValue && logBoxValue.switchLogBox) {
        setIsSwitchLog(logBoxValue.switchLogBox)
      }
    }
    loadFunc()

    const loadDsnFunc = async () => {
      const data = await load("CURRENT_DSN")
      console.log(`current dsn`, JSON.stringify(data))
      if (data && data.dsn) {
        setDsn(data.dsn)
        cloudOperation.switchDSN(data.dsn)
      }
    }
    loadDsnFunc()

    const subscription = DeviceEventEmitter.addListener("iotDebug", data => {
      // {name: "PbInput|PbOutput|ProbeTemp", src: "", dst: ""}
      if (!data) return
      if (data.name === "PbInput") {
        setPbInput(data)
      } else if (data.name === "PbOutput") {
        setPbOutput(data)
      } else if (data.name === "ProbeTemp") {
        setProbeTemp(data)
      } else if (data.name === "Warn") {
        setWarn(data)
      }
    })

    // setTimeout(() => {
    //   DeviceEventEmitter.emit("iotDebug", { name: "Warn", src: "bin3", dst: "tt" })
    // }, 1000 * 3)

    return () => {
      subscription.remove()
    }
  }, [])

  // Pull in navigation via hook
  // const navigation = useNavigation()
  return (
    <Screen style={ROOT} preset="fixed">
      <View style={{ alignItems: "center", justifyContent: "center" }}>
        <Text style={{ color: color.palette.titleColor, fontWeight: "bold" }}>Debug Mode</Text>
      </View>
      <View style={{
        marginVertical: 20,
        marginHorizontal: 20,
        justifyContent: "flex-start",
        // borderWidth: 1,
        height: windowHeight * 0.35
      }}>
        <View style={{
          marginBottom: 20,
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-between",
        }}>
          <Text style={{ color: color.palette.titleColor, fontWeight: "bold" }}>Switch IOT</Text>
          <Switch color={color.palette.green} value={isSwitchOn} onValueChange={onToggleSwitch} />
        </View>
        {/* <View style={{
          marginBottom: 20,
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-between",
        }}>
          <Text style={{ color: color.palette.titleColor, fontWeight: "bold" }}>Switch LogBox</Text>
          <Switch color={color.palette.green} value={isSwitchLog} onValueChange={onToggleSwitchLogBox} />
        </View> */}
        <View>
          <Text style={{ color: color.palette.titleColor, fontWeight: "bold", marginBottom: 5 }}>Switch DSN</Text>
          <DropDownPicker
            defaultValue={dsn}
            items={dsnItems}
            containerStyle={{ height: 40 }}
            style={{ backgroundColor: color.background }}
            itemStyle={{
              justifyContent: 'flex-start'
            }}
            dropDownStyle={{ backgroundColor: color.palette.white }}
            onChangeItem={item => onSelectDsnChange(item.value, item.index)}
          />
        </View>
      </View>
      <ScrollView style={{
        marginHorizontal: 20,
      }}>
        <View style={{ marginTop: 15 }}>
          <Text style={{ color: color.palette.titleColor, fontWeight: "bold", fontSize: 21 }}>PbInput</Text>
          <View>
            <Text style={{ color: color.palette.labelColor, fontSize: 18 }}>{pbInput.src}</Text>
          </View>
        </View>
        <View style={{ marginTop: 15 }}>
          <Text style={{ color: color.palette.titleColor, fontWeight: "bold", fontSize: 21 }}>PbOutput</Text>
          <View>
            <Text style={{ color: color.palette.labelColor, fontSize: 18 }}>{pbOutput.src}</Text>
          </View>
        </View>
        <View style={{ marginTop: 15 }}>
          <Text style={{ color: color.palette.titleColor, fontWeight: "bold", fontSize: 21 }}>ProbeTemp</Text>
          <View>
            <Text style={{ color: color.palette.labelColor, fontSize: 18 }}>{probeTemp.src}</Text>
          </View>
        </View>
        <View style={{ marginTop: 15 }}>
          <Text style={{ color: color.palette.titleColor, fontWeight: "bold", fontSize: 21 }}>Warn</Text>
          <View>
            <Text style={{ color: color.palette.labelColor, fontSize: 18 }}>{warn.src}</Text>
          </View>
        </View>
      </ScrollView>
    </Screen >
  )
})
