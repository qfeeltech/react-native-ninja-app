/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, Fragment } from "react"
import { observer } from "mobx-react-lite"
import {
  TextStyle,
  ViewStyle,
  View,
  Text,
  Dimensions,
  ScrollView,
  Image,
  TouchableOpacity,
} from "react-native"
import { Screen, RecipeCard } from "../../components"
import { useNavigation } from "@react-navigation/native"
// import { useStores } from "../../models"
import { color, style, typography } from "../../theme"
import Carousel from "react-native-snap-carousel"
import { RECIPES, FOOD_KEYS } from "../../data/foods"
import { getStatusBarHeight } from 'react-native-status-bar-height'

const windowWidth = Dimensions.get("window").width
const windowHeight = Dimensions.get("window").height

const ROOT: ViewStyle = {
  backgroundColor: color.palette.lightGrey200,
  flex: 1,
}

const BROWSE_CONTAINER: ViewStyle = {
  backgroundColor: color.palette.white,
  // marginTop: 20,
  // paddingTop: 20,
  // borderWidth: 1,
}

const TITLE_STYLE: ViewStyle = {
  // height: 50,
  // borderWidth: 1,
  marginTop: 72 - getStatusBarHeight(),
  justifyContent: "center",
  alignItems: "center",
  // marginBottom: 20,
}

const KEYS_STYLE: ViewStyle = { marginBottom: 40 }
const KEY_CONTAINER: ViewStyle = {
  // borderWidth: 1,
  flexDirection: "row",
  alignItems: "center",
  justifyContent: "space-around",
  marginHorizontal: 27,
}
const CAROUSEL_CONTAINER: ViewStyle = {
  // borderWidth: 1,
  width: windowWidth,
  height: windowHeight / 4.1855 + (40 * 2),
  // marginBottom: 20,
}
const EXPLORER_CONTAINER: ViewStyle = {}
const TEXT_LABEL_STYLE: TextStyle = {
  fontFamily: "Gotham-Book",
  fontSize: 16,
  // borderWidth: 1,
  letterSpacing: 0,
  lineHeight: 16,
  color: color.palette.lightGrey90,
}
const KEY_LABEL_STYLE: TextStyle = {
  fontSize: 16,
  fontFamily: "Gotham-Book",
  color: color.palette.labelColor,
}

const groupArray = (data, cols) => {
  return data
    .reduce(
      ([groups, subIndex], d) => {
        if (subIndex === 0) {
          groups.unshift([])
        }
        groups[0].push(d)
        return [groups, (subIndex + 1) % cols]
      },
      [[], 0],
    )[0]
    .reverse()
}

export const HomeScreen = observer(function HomeScreen() {
  // Pull in one of our MST stores
  // const { someStore, anotherStore } = useStores()

  const [recipes, setRecipes] = React.useState(RECIPES)
  const [carouselItems] = React.useState([
    {
      img: require("./chicken_2in.png"),
      name: "Chicken",
    },
    {
      img: require("./Beef_2in.png"),
      name: "Beef",
    },
    {
      img: require("./fish_2in.png"),
      name: "Fish",
    },
    {
      img: require("./pork.png"),
      name: "Pork",
    },
    {
      img: require("./Broccoli.png"),
      name: "Vegetable",
    },
    {
      img: require("./chicken_2in.png"),
      name: "Chicken",
    },
    {
      img: require("./Beef_2in.png"),
      name: "Beef",
    },
    {
      img: require("./fish_2in.png"),
      name: "Fish",
    },
    {
      img: require("./pork.png"),
      name: "Pork",
    },
    {
      img: require("./Broccoli.png"),
      name: "Vegetable",
    },
    {
      img: require("./chicken_2in.png"),
      name: "Chicken",
    },
    {
      img: require("./Beef_2in.png"),
      name: "Beef",
    },
    {
      img: require("./fish_2in.png"),
      name: "Fish",
    },
    {
      img: require("./pork.png"),
      name: "Pork",
    },
    {
      img: require("./Broccoli.png"),
      name: "Vegetable",
    },
    {
      img: require("./chicken_2in.png"),
      name: "Chicken",
    },
    {
      img: require("./Beef_2in.png"),
      name: "Beef",
    },
    {
      img: require("./fish_2in.png"),
      name: "Fish",
    },
    {
      img: require("./pork.png"),
      name: "Pork",
    },
    {
      img: require("./Broccoli.png"),
      name: "Vegetable",
    },
    {
      img: require("./chicken_2in.png"),
      name: "Chicken",
    },
    {
      img: require("./Beef_2in.png"),
      name: "Beef",
    },
    {
      img: require("./fish_2in.png"),
      name: "Fish",
    },
    {
      img: require("./pork.png"),
      name: "Pork",
    },
    {
      img: require("./Broccoli.png"),
      name: "Vegetable",
    },
    {
      img: require("./chicken_2in.png"),
      name: "Chicken",
    },
    {
      img: require("./Beef_2in.png"),
      name: "Beef",
    },
    {
      img: require("./fish_2in.png"),
      name: "Fish",
    },
    {
      img: require("./pork.png"),
      name: "Pork",
    },
    {
      img: require("./Broccoli.png"),
      name: "Vegetable",
    },
    {
      img: require("./chicken_2in.png"),
      name: "Chicken",
    },
    {
      img: require("./Beef_2in.png"),
      name: "Beef",
    },
    {
      img: require("./fish_2in.png"),
      name: "Fish",
    },
    {
      img: require("./pork.png"),
      name: "Pork",
    },
    {
      img: require("./Broccoli.png"),
      name: "Vegetable",
    },
    {
      img: require("./chicken_2in.png"),
      name: "Chicken",
    },
    {
      img: require("./Beef_2in.png"),
      name: "Beef",
    },
    {
      img: require("./fish_2in.png"),
      name: "Fish",
    },
    {
      img: require("./pork.png"),
      name: "Pork",
    },
    {
      img: require("./Broccoli.png"),
      name: "Vegetable",
    },
    {
      img: require("./chicken_2in.png"),
      name: "Chicken",
    },
    {
      img: require("./Beef_2in.png"),
      name: "Beef",
    },
    {
      img: require("./fish_2in.png"),
      name: "Fish",
    },
    {
      img: require("./pork.png"),
      name: "Pork",
    },
    {
      img: require("./Broccoli.png"),
      name: "Vegetable",
    },
    {
      img: require("./chicken_2in.png"),
      name: "Chicken",
    },
    {
      img: require("./Beef_2in.png"),
      name: "Beef",
    },
    {
      img: require("./fish_2in.png"),
      name: "Fish",
    },
    {
      img: require("./pork.png"),
      name: "Pork",
    },
    {
      img: require("./Broccoli.png"),
      name: "Vegetable",
    },
    {
      img: require("./chicken_2in.png"),
      name: "Chicken",
    },
    {
      img: require("./Beef_2in.png"),
      name: "Beef",
    },
    {
      img: require("./fish_2in.png"),
      name: "Fish",
    },
    {
      img: require("./pork.png"),
      name: "Pork",
    },
    {
      img: require("./Broccoli.png"),
      name: "Vegetable",
    },
    {
      img: require("./chicken_2in.png"),
      name: "Chicken",
    },
    {
      img: require("./Beef_2in.png"),
      name: "Beef",
    },
    {
      img: require("./fish_2in.png"),
      name: "Fish",
    },
    {
      img: require("./pork.png"),
      name: "Pork",
    },
    {
      img: require("./Broccoli.png"),
      name: "Vegetable",
    },
    {
      img: require("./chicken_2in.png"),
      name: "Chicken",
    },
    {
      img: require("./Beef_2in.png"),
      name: "Beef",
    },
    {
      img: require("./fish_2in.png"),
      name: "Fish",
    },
    {
      img: require("./pork.png"),
      name: "Pork",
    },
    {
      img: require("./Broccoli.png"),
      name: "Vegetable",
    },
    {
      img: require("./chicken_2in.png"),
      name: "Chicken",
    },
    {
      img: require("./Beef_2in.png"),
      name: "Beef",
    },
    {
      img: require("./fish_2in.png"),
      name: "Fish",
    },
    {
      img: require("./pork.png"),
      name: "Pork",
    },
    {
      img: require("./Broccoli.png"),
      name: "Vegetable",
    },
    {
      img: require("./chicken_2in.png"),
      name: "Chicken",
    },
    {
      img: require("./Beef_2in.png"),
      name: "Beef",
    },
    {
      img: require("./fish_2in.png"),
      name: "Fish",
    },
    {
      img: require("./pork.png"),
      name: "Pork",
    },
    {
      img: require("./Broccoli.png"),
      name: "Vegetable",
    },
    {
      img: require("./chicken_2in.png"),
      name: "Chicken",
    },
    {
      img: require("./Beef_2in.png"),
      name: "Beef",
    },
    {
      img: require("./fish_2in.png"),
      name: "Fish",
    },
    {
      img: require("./pork.png"),
      name: "Pork",
    },
    {
      img: require("./Broccoli.png"),
      name: "Vegetable",
    },
    {
      img: require("./chicken_2in.png"),
      name: "Chicken",
    },
    {
      img: require("./Beef_2in.png"),
      name: "Beef",
    },
    {
      img: require("./fish_2in.png"),
      name: "Fish",
    },
    {
      img: require("./pork.png"),
      name: "Pork",
    },
    {
      img: require("./Broccoli.png"),
      name: "Vegetable",
    },
    {
      img: require("./chicken_2in.png"),
      name: "Chicken",
    },
    {
      img: require("./Beef_2in.png"),
      name: "Beef",
    },
    {
      img: require("./fish_2in.png"),
      name: "Fish",
    },
    {
      img: require("./pork.png"),
      name: "Pork",
    },
    {
      img: require("./Broccoli.png"),
      name: "Vegetable",
    },
    {
      img: require("./chicken_2in.png"),
      name: "Chicken",
    },
    {
      img: require("./Beef_2in.png"),
      name: "Beef",
    },
    {
      img: require("./fish_2in.png"),
      name: "Fish",
    },
    {
      img: require("./pork.png"),
      name: "Pork",
    },
    {
      img: require("./Broccoli.png"),
      name: "Vegetable",
    },
    {
      img: require("./chicken_2in.png"),
      name: "Chicken",
    },
    {
      img: require("./Beef_2in.png"),
      name: "Beef",
    },
    {
      img: require("./fish_2in.png"),
      name: "Fish",
    },
    {
      img: require("./pork.png"),
      name: "Pork",
    },
    {
      img: require("./Broccoli.png"),
      name: "Vegetable",
    },
    {
      img: require("./chicken_2in.png"),
      name: "Chicken",
    },
    {
      img: require("./Beef_2in.png"),
      name: "Beef",
    },
    {
      img: require("./fish_2in.png"),
      name: "Fish",
    },
    {
      img: require("./pork.png"),
      name: "Pork",
    },
    {
      img: require("./Broccoli.png"),
      name: "Vegetable",
    },
    {
      img: require("./chicken_2in.png"),
      name: "Chicken",
    },
    {
      img: require("./Beef_2in.png"),
      name: "Beef",
    },
    {
      img: require("./fish_2in.png"),
      name: "Fish",
    },
    {
      img: require("./pork.png"),
      name: "Pork",
    },
    {
      img: require("./Broccoli.png"),
      name: "Vegetable",
    },
    {
      img: require("./chicken_2in.png"),
      name: "Chicken",
    },
    {
      img: require("./Beef_2in.png"),
      name: "Beef",
    },
    {
      img: require("./fish_2in.png"),
      name: "Fish",
    },
    {
      img: require("./pork.png"),
      name: "Pork",
    },
    {
      img: require("./Broccoli.png"),
      name: "Vegetable",
    },
    {
      img: require("./chicken_2in.png"),
      name: "Chicken",
    },
    {
      img: require("./Beef_2in.png"),
      name: "Beef",
    },
    {
      img: require("./fish_2in.png"),
      name: "Fish",
    },
    {
      img: require("./pork.png"),
      name: "Pork",
    },
    {
      img: require("./Broccoli.png"),
      name: "Vegetable",
    },
    {
      img: require("./chicken_2in.png"),
      name: "Chicken",
    },
    {
      img: require("./Beef_2in.png"),
      name: "Beef",
    },
    {
      img: require("./fish_2in.png"),
      name: "Fish",
    },
    {
      img: require("./pork.png"),
      name: "Pork",
    },
    {
      img: require("./Broccoli.png"),
      name: "Vegetable",
    },
    {
      img: require("./chicken_2in.png"),
      name: "Chicken",
    },
    {
      img: require("./Beef_2in.png"),
      name: "Beef",
    },
    {
      img: require("./fish_2in.png"),
      name: "Fish",
    },
    {
      img: require("./pork.png"),
      name: "Pork",
    },
    {
      img: require("./Broccoli.png"),
      name: "Vegetable",
    },
    {
      img: require("./chicken_2in.png"),
      name: "Chicken",
    },
    {
      img: require("./Beef_2in.png"),
      name: "Beef",
    },
    {
      img: require("./fish_2in.png"),
      name: "Fish",
    },
    {
      img: require("./pork.png"),
      name: "Pork",
    },
    {
      img: require("./Broccoli.png"),
      name: "Vegetable",
    },
    {
      img: require("./chicken_2in.png"),
      name: "Chicken",
    },
    {
      img: require("./Beef_2in.png"),
      name: "Beef",
    },
    {
      img: require("./fish_2in.png"),
      name: "Fish",
    },
    {
      img: require("./pork.png"),
      name: "Pork",
    },
    {
      img: require("./Broccoli.png"),
      name: "Vegetable",
    },
    {
      img: require("./chicken_2in.png"),
      name: "Chicken",
    },
    {
      img: require("./Beef_2in.png"),
      name: "Beef",
    },
    {
      img: require("./fish_2in.png"),
      name: "Fish",
    },
    {
      img: require("./pork.png"),
      name: "Pork",
    },
    {
      img: require("./Broccoli.png"),
      name: "Vegetable",
    },
    {
      img: require("./chicken_2in.png"),
      name: "Chicken",
    },
    {
      img: require("./Beef_2in.png"),
      name: "Beef",
    },
    {
      img: require("./fish_2in.png"),
      name: "Fish",
    },
    {
      img: require("./pork.png"),
      name: "Pork",
    },
    {
      img: require("./Broccoli.png"),
      name: "Vegetable",
    },
    {
      img: require("./chicken_2in.png"),
      name: "Chicken",
    },
    {
      img: require("./Beef_2in.png"),
      name: "Beef",
    },
    {
      img: require("./fish_2in.png"),
      name: "Fish",
    },
    {
      img: require("./pork.png"),
      name: "Pork",
    },
    {
      img: require("./Broccoli.png"),
      name: "Vegetable",
    },
    {
      img: require("./chicken_2in.png"),
      name: "Chicken",
    },
    {
      img: require("./Beef_2in.png"),
      name: "Beef",
    },
    {
      img: require("./fish_2in.png"),
      name: "Fish",
    },
    {
      img: require("./pork.png"),
      name: "Pork",
    },
    {
      img: require("./Broccoli.png"),
      name: "Vegetable",
    },
  ])
  const [firstItem] = React.useState(1)
  const [foodName, setFoodName] = React.useState(carouselItems[firstItem].name)
  const [foodKeys, setFoodKeys] = React.useState(FOOD_KEYS[carouselItems[firstItem].name])

  useEffect(() => {
    console.log(`Browse Ingredients`)
    console.log(getStatusBarHeight())
    console.log(carouselItems.length)

    return () => {
      console.log('')
    }
  }, [])

  const onSnapToItem = (index) => {
    console.log(`slideIndex: ${index}`)
    const newItem = carouselItems[index]
    setFoodName(newItem.name)
    setFoodKeys(FOOD_KEYS[newItem.name])
  }

  // Pull in navigation via hook
  const navigation = useNavigation()

  const renderItem = ({ item }) => {
    return (
      <View style={{ alignItems: "center", justifyContent: "center" }}>
        <Image style={{ width: windowHeight * 0.24, height: windowHeight * 0.24 }} source={item.img} />
      </View>
    )
  }

  return (
    <Screen style={ROOT} preset="scroll">
      <ScrollView>
        <View style={BROWSE_CONTAINER}>
          <View style={[TITLE_STYLE, {}]}>
            <View style={{ height: 16, marginBottom: 9 }}>
              <Text
                style={{
                  fontFamily: "Gotham-Book",
                  fontSize: 16,
                  // borderWidth: 0.5,
                  letterSpacing: 0,
                  lineHeight: 16,
                  borderColor: color.palette.angry,
                  color: color.palette.labelColor
                }}
              >
                Browse Ingredients
              </Text>
            </View>
            <View style={{
              // backgroundColor: color.palette.borderColor,
              height: 21
            }}>
              <Text
                style={{
                  textAlign: "center",
                  fontFamily: typography.bold,
                  fontSize: 21,
                  // borderWidth: 1,
                  letterSpacing: 0,
                  lineHeight: 21,
                  borderColor: color.palette.green,
                  color: color.palette.labelColor10
                }}
              >
                {foodName}
              </Text>
            </View>
          </View>
          <View style={CAROUSEL_CONTAINER}>
            <Carousel
              loop={true}
              zIndex={10}
              enableSnap={true}
              data={carouselItems}
              sliderWidth={windowWidth}
              itemWidth={windowHeight / 4.1855}
              renderItem={renderItem}
              slideStyle={{
                alignItems: "center",
                justifyContent: "center",
                ...style.IMAGE_SHADOW_STYLE,
              }}
              firstItem={firstItem}
              onSnapToItem={(slideIndex) => onSnapToItem(slideIndex)}
            />
          </View>
          {/* keys */}
          <View style={KEYS_STYLE}>
            {groupArray(foodKeys, 3).map((keys, index) => {
              return (
                <View key={index}>
                  <View style={KEY_CONTAINER}>
                    {keys.map((item, index0) => {
                      return (
                        <Fragment key={index0}>
                          <View style={{ alignItems: "center", width: windowWidth * 0.25, height: 45, justifyContent: "center" }}>
                            <TouchableOpacity onPress={() => {
                              if (foodName === 'Beef' && item === 'Fillet') {
                                navigation.navigate("bottomTabsA", {
                                  name: item
                                })
                              }
                            }}>
                              <Text style={KEY_LABEL_STYLE}>{item}</Text>
                            </TouchableOpacity>
                          </View>
                          {index0 !== keys.length - 1 ? <View style={{ backgroundColor: color.palette.lightGrey10, width: 1, height: 45 }}></View> : null}
                        </Fragment>
                      )
                    })}
                  </View>
                  {index !== (groupArray(foodKeys, 3).length - 1) ? <View style={KEY_CONTAINER}>
                    <View style={{ backgroundColor: color.palette.lightGrey10, width: 80, height: 1 }}></View>
                    <View style={{ backgroundColor: color.palette.lightGrey10, width: 1, height: 1 }}></View>
                    <View style={{ backgroundColor: color.palette.lightGrey10, width: 80, height: 1 }}></View>
                    <View style={{ backgroundColor: color.palette.lightGrey10, width: 1, height: 1 }}></View>
                    <View style={{ backgroundColor: color.palette.lightGrey10, width: 80, height: 1 }}></View>
                  </View> : null}
                </View>
              )
            })}
          </View>
        </View>
        <View style={EXPLORER_CONTAINER}>
          <View style={{ alignItems: "center", justifyContent: "center", marginVertical: 11, height: 16, }}>
            <Text style={TEXT_LABEL_STYLE}>Explore Recipes</Text>
          </View>
          <View style={{ alignItems: "center" }}>
            {recipes.filter(item => item.type === foodName).map((item, index) => {
              return (
                <RecipeCard
                  key={index}
                  onPress={() => {
                    if (item.title === 'Grilled fillet and cauliflower steak') {
                      navigation.navigate("bottomTabsB")
                    }
                  }}
                  title={item.title}
                  source={item.source}
                  cookTime={item.time} />
              )
            })}
          </View>
        </View>
      </ScrollView>
    </Screen>
  )
})
