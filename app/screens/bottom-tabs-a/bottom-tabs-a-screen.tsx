import React from "react"
import { observer } from "mobx-react-lite"
import { BottomNavigation } from "../../components"
import { CookSettingsScreen, DebugModeScreen, DemoScreen } from "../../screens"

export const BottomTabsAScreen = observer(function BottomTabsScreen() {
  const sceneMap = {
    home: CookSettingsScreen,
    user: DebugModeScreen,
    mark: DemoScreen,
    search: DemoScreen,
  }

  return (
    <BottomNavigation renderScene={sceneMap} />
  )
})
