/* eslint-disable react-native/no-inline-styles */
import React, { useState } from "react"
import { observer } from "mobx-react-lite"
import {
  ViewStyle,
  ScrollView,
} from "react-native"
import { Screen, CookSettings, CookHeader } from "../../components"
import { useNavigation } from "@react-navigation/native"
// import { useStores } from "../../models"
import { color, typography } from "../../theme"

const ROOT: ViewStyle = {
  backgroundColor: color.palette.white,
  flex: 1,
}
const STEAK_IMG = require("./steak0.png")
interface CookSettingsProps {
  cookTime: number
  doneness: number
  thickness: number
  source: any
  ingredients: string[]
}

const COOKBOOK_SUMMARY: CookSettingsProps = {
  cookTime: 40,
  doneness: 5,
  thickness: 1,
  source: require("./steak0.png"),
  ingredients: [
    "Ribeye steak",
    "1/2 tablespoon canola oil",
    "Cauliflower",
    "1/3 cup olives",
    "1/2 cup roasted red peppers",
    "1 tbsp minced oregano",
    "1 tbsp minced parsley",
    "3 minced garlic cloves",
    "1 lemon (juice)",
    "1lb crumbled feta",
    "Salt and pepper",
  ],
}

export const CookDetailScreen = observer(function CookDetailScreen() {
  // Pull in one of our MST stores
  // const { someStore, anotherStore } = useStores()

  // Pull in navigation via hook
  const navigation = useNavigation()

  const [cookSettings] = useState(COOKBOOK_SUMMARY)

  const onChangeSliderValue = (value: number) => {
    console.log(value)
  }

  const setThickness = (index) => {
    console.log(index)
  }

  return (
    <Screen style={ROOT} preset="scroll">
      <ScrollView>
        <CookHeader
          title="Grilled ribeye and cauliflower steak"
          titleStyle={{ fontFamily: typography.bold, fontSize: 21 }}
          onLeftPress={() => navigation.goBack()} />
        <CookSettings
          sliderValue={6}
          source={STEAK_IMG}
          contentStyle={{ marginHorizontal: 27 }}
          itemStyle={{ marginHorizontal: 17 }}
          ingredients={cookSettings.ingredients}
          onPress={() => navigation.navigate("cook")}
        />
      </ScrollView>
    </Screen>
  )
})
