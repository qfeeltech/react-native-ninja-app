/* eslint-disable react-native/no-inline-styles */
import React from "react"
import { observer } from "mobx-react-lite"
import {
  ViewStyle,
  Image, View,
  Dimensions,
  ScrollView,
  ActivityIndicator,
  TouchableOpacity,
  TouchableWithoutFeedback,
  TextInput
} from "react-native"
import { Screen, CookHeader, Button, Text } from "../../components"
import { useNavigation } from "@react-navigation/native"
// import { useStores } from "../../models"
import { color, style, typography } from "../../theme"
import Modal from 'react-native-modal'
import cloudOperation from '../../ayla_interface/cloudOperation.js'
import { cloneDeep } from 'lodash'

const windowWidth = Dimensions.get("window").width
const windowHeight = Dimensions.get("window").height
const BUTTON_WIDTH = windowWidth - (44 * 2)

const ROOT: ViewStyle = {
  backgroundColor: color.palette.white,
  flex: 1,
}

const NAV_STYLE: ViewStyle = {
  justifyContent: "center",
  height: 25,
  width: 25
}

const wifiList = [
  "qfeeltech",
  "sharkninja",
  "Sharkroot",
  "Sharkroot_5G",
  "Sharkroot_2.4G",
  "eeword",
  "Home1",
  "attwifi",
  "test",
  "ceshi"
]

const MODAL_INIT = {
  visible: false,
  height: windowHeight / 2.67, // 1.64, 2.67
  title: "Choose a network",
  leftPress: false,
  rightPress: false,
  steps: [
    {
      visible: false,
      title: "Choose a network",
      height: windowHeight / 1.64,
      rightPress: true
    },
    {
      visible: false,
      title: "Enter password",
      height: windowHeight / 2.67,
      leftPress: true,
      rightPress: true
    },
    { visible: false, title: "Connecting", height: windowHeight / 2.67, loader: true },
    { visible: false, title: "Connected", height: windowHeight / 2.67 },
  ],
}

export const PairingScreen = observer(function PairingScreen() {
  // Pull in one of our MST stores
  // const { someStore, anotherStore } = useStores()

  const [modal, setModal] = React.useState(cloneDeep(MODAL_INIT))
  const hideModal = () => setModal(cloneDeep(MODAL_INIT))

  React.useEffect(() => {
    const steps = modal.steps
    if (steps[2].visible) {
      console.log("Countdown timer")
      setTimeout(() => onNextStep(3), 1000 * 5)
    } else if (steps[3].visible) {
      console.log("Connected")
      setTimeout(() => {
        hideModal()
        navigation.navigate("paringComplete")
      }, 1000 * 3)
    }
  }, [modal])

  const onNextStep = (index) => {
    console.log(`onNextStep`)
    setModal((oldItem) => {
      const newItem = { ...oldItem }
      newItem.visible = true
      for (let i = 0; i < modal.steps.length; i++) {
        const step = modal.steps[i]
        if (i === index) {
          step.visible = true
          newItem.title = step.title
          newItem.height = step.height
          if (step.leftPress) {
            newItem.leftPress = true
          } else {
            newItem.leftPress = false
          }
          if (step.rightPress) {
            newItem.rightPress = true
          } else {
            newItem.rightPress = false
          }
        } else {
          step.visible = false
        }
      }
      return newItem
    })
  }

  // Pull in navigation via hook
  const navigation = useNavigation()

  return (
    <Screen style={ROOT} preset="scroll">
      <CookHeader
        style={{ height: 50 }}
        titleStyle={{ fontFamily: "Gotham-Bold", fontSize: 21 }}
        title="Pairing"
      />
      <View style={{ marginTop: 111, alignItems: "center" }}>
        <Image style={{
          height: windowHeight / 3.776,
        }} source={require("./grill.png")}
        />
      </View>
      <View style={{ flex: 1, alignItems: "center", marginHorizontal: 44, marginTop: 100 }}>
        <Text style={{
          color: color.palette.titleColor,
          fontSize: 16,
          fontFamily: typography.bold,
        }}>Foodi Smart Indoor Grill detected</Text>
        <View style={{ marginTop: 20 }}>
          <Text style={{ fontSize: 16, color: color.palette.labelColor, }}>
            Is this the product to pair?
          </Text>
        </View>
        <View style={{ marginTop: 33 }}>
          <Button
            onPress={() => onNextStep(0)}
            style={{
              ...style.SHADOW_STYLE,
              width: BUTTON_WIDTH,
              height: 48,
              marginBottom: 20,
            }}>
            <Text style={{ fontFamily: "Gotham-Book", fontSize: 19 }}>Yes, Pair!</Text>
          </Button>
          <Button
            disabled={true}
            onPress={() => console.log("HeHe")}
            style={{
              ...style.SHADOW_STYLE,
              width: BUTTON_WIDTH,
              height: 48,
              marginBottom: 20,
              backgroundColor: color.palette.white,
              borderWidth: 1,
              borderColor: color.palette.buttonColor
            }}>
            <Text style={{ fontFamily: "Gotham-Book", fontSize: 19, color: color.palette.green }}>Cancel</Text>
          </Button>
        </View>
      </View>
      <Modal
        isVisible={modal.visible}
        animationIn={"zoomIn"}
        animationOutTiming={1000 * 0.3}
        animationOut={"zoomOut"}
        customBackdrop={
          <TouchableWithoutFeedback onPress={hideModal}>
            <View style={{ flex: 1, backgroundColor: color.palette.backdropColor }} />
          </TouchableWithoutFeedback>
        }
        style={{
          marginHorizontal: 17,
          borderRadius: 12,
          maxHeight: modal.height,
          marginTop: windowHeight / 5.075,
          backgroundColor: color.palette.white,
          justifyContent: "flex-start"
        }}>
        <View style={{
          justifyContent: "space-between",
          flexDirection: "row",
          // borderWidth: 1,
          marginHorizontal: 20,
          marginTop: 20,
          height: windowHeight / 11.6,
        }}>
          <TouchableOpacity style={NAV_STYLE} onPress={() => onNextStep(0)}>
            {modal.leftPress ? <Image style={{ width: 18, height: 25 }} source={require("./arrow-back.png")} /> : null}
          </TouchableOpacity>
          <View style={{
            alignItems: "center"
          }}>
            <Text style={{ color: color.palette.titleColor, fontSize: 21, fontFamily: typography.bold, }}>Wi-Fi</Text>
            <Text style={{ color: color.palette.labelColor, fontSize: 12, fontWeight: "400", marginTop: 13 }}>{modal.title}</Text>
          </View>
          <TouchableOpacity style={NAV_STYLE} onPress={() => hideModal()}>
            {modal.rightPress ? <Image style={{ width: 18, height: 18 }} source={require("./closed.png")} /> : null}
          </TouchableOpacity>
        </View>
        {modal.steps[0].visible ? <View style={{
          height: windowHeight / 2.15,
          // borderWidth: 0.5,
          // borderColor: color.palette.buttonColor,
          marginHorizontal: 20,
        }}>
          <ScrollView>
            {wifiList.map((val, index) => {
              return (
                <TouchableOpacity
                  onPress={() => onNextStep(1)}
                  key={index}
                  style={[{
                    height: 60,
                    alignItems: "center",
                    justifyContent: "space-between",
                    flexDirection: "row",
                  },
                  index !== (wifiList.length - 1) ? {
                    borderBottomWidth: 1,
                    borderBottomColor: color.palette.lightGrey10,
                  } : {}]}>
                  <View>
                    <Text style={{ color: color.palette.titleColor, fontWeight: "400", fontSize: 16 }}>{val}</Text>
                  </View>
                  <View style={{ flexDirection: "row" }}>
                    <Image source={require("./lock.png")} />
                    <Image style={{ marginHorizontal: 10 }} source={require("./wifi.png")} />
                    <Image style={{ marginRight: 10 }} source={require("./mark.png")} />
                  </View>
                </TouchableOpacity>
              )
            })}
          </ScrollView>
        </View> : null}
        {modal.steps[1].visible ? <View style={{
          height: windowHeight / 4.2,
          // borderWidth: 1,
          alignItems: "center"
        }}>
          <TextInput
            secureTextEntry={true}
            style={{
              width: windowWidth / 1.24,
              marginBottom: 20,
              height: 48,
              paddingHorizontal: 30,
              borderRadius: 24,
              borderWidth: 1,
              borderColor: color.palette.borderColor
            }} />
          <Button
            onPress={() => { onNextStep(2); cloudOperation.setWifi(true) }}
            style={{
              ...style.SHADOW_STYLE,
              width: windowWidth / 1.24,
              height: 48,
              marginBottom: 20,
            }}>
            <Text style={{ fontFamily: "Gotham-Book", fontSize: 19 }}>Join</Text>
          </Button>
          <Button
            onPress={() => { console.log("Cancel"); onNextStep(0); cloudOperation.setWifi(false) }}
            style={{
              ...style.SHADOW_STYLE,
              width: windowWidth / 1.24,
              height: 48,
              marginBottom: 20,
              backgroundColor: color.palette.white,
              borderWidth: 1,
              borderColor: color.palette.buttonColor
            }}>
            <Text style={{
              fontFamily: "Gotham-Book",
              fontSize: 19,
              color: color.palette.green
            }}>Cancel</Text>
          </Button>
        </View> : null}
        {modal.steps[2].visible ? <View style={{
          height: windowHeight / 4.2,
          // borderWidth: 1,
          justifyContent: "center",
          alignItems: "center",
        }}>
          <ActivityIndicator size="large" color={color.palette.green} />
        </View> : null}
        {modal.steps[3].visible ? <View style={{
          height: windowHeight / 4.2,
          justifyContent: "center",
          alignItems: "center",
        }}>
          <Image source={require("./complete.png")} />
        </View> : null}
      </Modal>
    </Screen >
  )
})
