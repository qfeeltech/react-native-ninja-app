/* eslint-disable react-native/no-inline-styles */
import React, { useEffect } from "react"
import { observer } from "mobx-react-lite"
import { ViewStyle, View, Image, Dimensions } from "react-native"
import { Screen, CookHeader, Text } from "../../components"
import { useNavigation } from "@react-navigation/native"
// import { useStores } from "../../models"
import { color, typography } from "../../theme"
import cloudOperation from "../../ayla_interface/cloudOperation.js"

const windowWidth = Dimensions.get("window").width
const windowHeight = Dimensions.get("window").height

const ROOT: ViewStyle = {
  backgroundColor: color.palette.white,
  flex: 1,
}

export const SetupNinjaScreen = observer(function SetupNinjaScreen() {
  // Pull in one of our MST stores
  // const { someStore, anotherStore } = useStores()

  const timeoutRef = React.useRef(undefined)

  // Pull in navigation via hook
  const navigation = useNavigation()

  useEffect(() => {
    console.log("Setup Ninja Screen")
    timeoutRef.current = setInterval(() => {
      timeoutRef.current && clearInterval(timeoutRef.current)
      const { index, routes } = navigation.dangerouslyGetState()
      const currentRoute = routes[index].name
      console.log("current screen", currentRoute)
      if (currentRoute === "setup") {
        navigation.navigate("pairing")
        cloudOperation.setPairing()
        console.log("timeout 15s nav to pairing screen")
      }
    }, 1000 * 3 * 2)

    return () => {
      console.log("Setup Ninja Screen close.")
      timeoutRef.current && clearInterval(timeoutRef.current)
    }
  }, [])

  const _goBack = () => {
    timeoutRef.current && clearInterval(timeoutRef.current)
    navigation.goBack()
  }

  return (
    <Screen style={ROOT} preset="fixed">
      <CookHeader
        titleStyle={{
          fontFamily: "Gotham-Bold",
          fontSize: 21
        }}
        title="Setup Ninja"
        onLeftPress={() => _goBack()} />
      <View style={{ alignItems: "center", justifyContent: "center", marginTop: 100 }}>
        <Image
          style={{
            // borderWidth: 1,
            width: windowWidth,
            resizeMode: "center"
          }}
          source={require("./power.png")}
        />
      </View>
      <View style={{ flex: 1, alignItems: "center", marginHorizontal: 44, marginTop: 100 }}>
        <Text style={{ fontFamily: typography.bold, color: color.palette.titleColor, fontSize: 16 }}>
          Product plugged in and ready
        </Text>
        <View style={{ marginTop: 21 }}>
          <Text style={{
            fontFamily: "Gotham-Book",
            color: color.palette.labelColor,
            lineHeight: 22,
            fontSize: 16,
            textAlign: "center",
            fontWeight: '400'
          }}>
            Press the power button on your Ninja to setup. Make sure your phone is connected to a Wi-Fi network and you have the Wi-Fi password
            ready.
          </Text>
        </View>
      </View>
    </Screen>
  )
})
