/* eslint-disable react-native/no-inline-styles */
import React from "react"
import { observer } from "mobx-react-lite"
import { ViewStyle, TextStyle, View, Text, Dimensions, ScrollView, TouchableOpacity, Image } from "react-native"
import { Screen, CookSettings, RecipeCard } from "../../components"
import { useNavigation, StackActions } from "@react-navigation/native"
// import { useStores } from "../../models"
import { color, style } from "../../theme"
import { RECIPES } from "../../data/foods"
import { getStatusBarHeight } from 'react-native-status-bar-height'

const windowWidth = Dimensions.get("window").width
// const windowHeight = Dimensions.get("window").height

const ROOT: ViewStyle = {
  backgroundColor: color.palette.white,
  flex: 1,
}
const TEXT_LABEL: TextStyle = { fontFamily: "Gotham-Book", fontSize: 16 }

interface CookSettingsProps {
  route: { key: string, name: string, params: { name: string } }
}

const NAV_ICON_SIZE = 25

export const CookSettingsScreen = observer(function CookSettingsScreen(props: CookSettingsProps) {
  // Pull in one of our MST stores
  // const { someStore, anotherStore } = useStores()

  const { route } = props

  const [title, setTitle] = React.useState("Fillet")
  const [sliderValue, setSliderValue] = React.useState(4)

  // Pull in navigation via hook
  const navigation = useNavigation()

  React.useEffect(() => {
    if (route && route.params) {
      setTitle(route.params.name)
    }
  }, [])

  const [recipes] = React.useState(RECIPES) // TODO

  return (
    <Screen style={ROOT} preset="fixed">
      <ScrollView style={{ backgroundColor: color.palette.lightGrey200 }}>
        <View style={{
          flexDirection: "row",
          backgroundColor: color.palette.white,
          justifyContent: "space-between",
          alignItems: "center",
          paddingTop: 67 - getStatusBarHeight(),
          paddingHorizontal: 27,
          paddingBottom: 5,
        }}>
          {/* navigation.dispatch(StackActions.replace("bottomTabs")) */}
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Image style={{ width: NAV_ICON_SIZE, height: NAV_ICON_SIZE }} source={require("./arrow-back.png")} />
          </TouchableOpacity>
          <View style={{ alignItems: "center", justifyContent: "center" }}>
            <Text style={{
              textAlign: "center",
              fontSize: 16,
              letterSpacing: 0,
              lineHeight: 16,
              color: color.palette.labelColor
            }}>Cook Settings</Text>
          </View>
          <View style={{ width: NAV_ICON_SIZE, height: NAV_ICON_SIZE }}></View>
        </View>
        <CookSettings
          sliderValue={sliderValue}
          onChangeSliderValue={(value) => setSliderValue(value)}
          imageWidth={windowWidth / 2.467}
          imageHeight={windowWidth / 2.467}
          imageStyle={{ marginVertical: 32, ...style.IMAGE_SHADOW_STYLE, }}
          onPress={() => console.log('HeHe')}
          title={title}
          buttonLabel={"Start cooking"}
          source={require("./Beef_2in.png")}
        />
        <View>
          <View style={{ alignItems: "center", justifyContent: "center", marginVertical: 12 }}>
            <Text style={[TEXT_LABEL, { color: color.palette.lightGrey20 }]}>Explore Recipes</Text>
          </View>
          <View style={{ alignItems: "center" }}>
            {recipes.filter(item => item.title.toLowerCase().indexOf(title.toLowerCase()) !== -1).map((item, index) => {
              return (
                <RecipeCard
                  key={index}
                  onPress={() => {
                    if (item.title === 'Grilled fillet and cauliflower steak') {
                      navigation.navigate("bottomTabsB")
                    }
                  }}
                  title={item.title}
                  source={item.source}
                  cookTime={item.time} />
              )
            })}
          </View>
        </View>
      </ScrollView>
    </Screen>
  )
})
