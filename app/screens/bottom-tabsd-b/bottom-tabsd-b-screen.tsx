import React from "react"
import { observer } from "mobx-react-lite"
import { BottomNavigation } from "../../components"
import { CookScreen, DebugModeScreen, DemoScreen } from "../../screens"

export const BottomTabsBScreen = observer(function BottomTabsScreen() {
  const sceneMap = {
    home: CookScreen,
    user: DebugModeScreen,
    mark: DemoScreen,
    search: DemoScreen,
  }

  return (
    <BottomNavigation renderScene={sceneMap} />
  )
})
