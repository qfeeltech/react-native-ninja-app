import React from "react"
import { observer } from "mobx-react-lite"
import { BottomNavigation } from "../../components"
import { HomeScreen, DebugModeScreen, DemoScreen } from "../../screens"

export const BottomTabsScreen = observer(function BottomTabsScreen() {
  const ss = {
    home: HomeScreen,
    user: DebugModeScreen,
    mark: DemoScreen,
    search: DemoScreen,
  }

  return (
    <BottomNavigation renderScene={ss} />
  )
})
