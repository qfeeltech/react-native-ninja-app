/* eslint-disable react-native/no-inline-styles */
import React, { useState, useEffect, useRef, Fragment } from "react"
import { observer } from "mobx-react-lite"
import {
  ViewStyle,
  ScrollView,
  View,
  ImageStyle,
  TextStyle,
  Image,
  Dimensions,
  TouchableOpacity,
  DeviceEventEmitter,
  ImageBackground,
  findNodeHandle,
} from "react-native"
import { Screen, CookHeader, CookSettings, Button, Text, Modal } from "../../components"
import { useNavigation, StackActions } from "@react-navigation/native"
// import { useStores } from "../../models"
import { Card } from "react-native-paper"
import * as Progress from "react-native-progress"
import { color, style } from "../../theme"
import cloudOperation from "../../ayla_interface/cloudOperation"
// import PushNotification from "react-native-push-notification"
import { load } from "../../utils/storage"
import { cloneDeep } from 'lodash'

const windowWidth = Dimensions.get("window").width
const windowHeight = Dimensions.get("window").height

console.log(`width: ${windowWidth}, height: ${windowHeight}`)
const IMAGE_WIDTH = windowHeight / 2.88
const PROGRESS_BAR_COLOR = color.palette.lightGrey100
const PROGRESS_BAR_WIDTH = windowWidth - 44 * 2
const PROGRESS_BAR_SPACE = 20
const PROGRESS_BAR_SPACE1 = 13
const ROOT: ViewStyle = {
  backgroundColor: "#F8F8F8",
  flex: 1,
}

const STEAK_IMG = require("./steak0.png")

interface CookingStepsProps {
  step: number
  summary: {
    title: string
    desc: string[]
  }[]
  source?: any
  disabled?: boolean
  helper?: {
    title: string
    source: any
  }
  type?: number
  progressBar: {
    type: number
    hideProgress?: boolean
    countdownTimer?: boolean
    status?: number
    disabled?: boolean
    direction?: number
    showMessages?: boolean
    prompt?: string
    showButton: boolean
    endValue?: number
    value?: number
    messages?: { label: string; val: any; unit: string, flag?: number }[]
  }[]
  notification?: { title: string, message: string }
}

interface CookSettingsProps {
  cookTime: number
  doneness: number
  thickness: number
  source: any
  ingredients: string[]
}

const COOKBOOK_SUMMARY: CookSettingsProps = {
  cookTime: 40,
  doneness: 5,
  thickness: 1,
  source: require("./steak0.png"),
  ingredients: [
    "Fillet steak",
    "1/2 tablespoon canola oil",
    "Cauliflower",
    "1/3 cup olives",
    "1/2 cup roasted red peppers",
    "1 tbsp minced oregano",
    "1 tbsp minced parsley",
    "3 minced garlic cloves",
    "1 lemon (juice)",
    "1lb crumbled feta",
    "Salt and pepper",
  ],
}

const COOKING_STEPS: CookingStepsProps[] = [
  {
    step: 1,
    disabled: false,
    summary: [
      {
        title: "1. Prep steak",
        desc: [
          "Brush each steak on all sides with canola oil.",
          "Season with salt and pepper as desired.",
        ],
      },
      {
        title: "You'll need:",
        desc: ["Fillet steak", "1/2 tablespoon canola oil", "salt and pepper"],
      },
    ],
    source: require("./steak1.png"),
    progressBar: [
      {
        type: 0, // control
        prompt: "Step 1 complete",
        showButton: true,
      },
    ],
  },
  {
    step: 2,
    disabled: false,
    summary: [
      {
        title: "2. Preheat unit",
        desc: ["Press start/stop on unit to start preheating."],
      },
    ],
    progressBar: [
      {
        type: 0, // control
        prompt: "Ready to Preheat",
        showButton: false,
        hideProgress: true,
        value: 0,
        endValue: 8,
      },
    ],
  },
  {
    step: 3,
    disabled: false,
    summary: [
      {
        title: "3. Prep cauliflower and salsa",
        desc: [
          `Cut cauliflower from top to bottom into two 3/4-1 inch "steaks"; reserve remaining cauliflower.`,
          `Season with salt and pepper as desired.`,
          `To make the Greek salsa, in a large bowl, stir together olives, roasted red peppers, oregano, parsley, garlic, lemon juice, feta, salt, pepper, walnuts, red onion, and 2 tablespoons of canola oil.`,
          `Set Aside.`,
        ],
      },
    ],
    source: require("./steak_3.png"),
    type: 1,
    progressBar: [
      {
        type: 0, // control
        prompt: "Step 3 complete",
        showButton: true,
      },
      {
        type: 0, // control
        prompt: "Unit preheating",
        showMessages: true,
        messages: [
          { label: "Unit preheating", val: 0, unit: "%" },
        ],
        showButton: false,
        value: 0,
        endValue: 8,
      },
    ],
    notification: {
      title: "Preheat complete!",
      message: "Ready to add steak."
    }
  },
  {
    step: 4,
    disabled: false,
    summary: [
      {
        title: "4. Add steak",
        desc: [
          `Set up thermometer with steak and place both in grill.`,
          `Place steak on grill plate.`,
          `Close lid.`,
        ],
      },
      {
        title: "You'll need:",
        desc: [`Fillet steak`, `Thermometer`],
      },
    ],
    helper: {
      title: "Tips for thermometer",
      source: require("./steak_4_1.png"),
    },
    source: require("./steak_4.png"),
    progressBar: [
      {
        type: 0, // control
        prompt: "Step 4 complete",
        showButton: true,
        direction: 1,
        showMessages: false,
        messages: [
          { label: "Current temp", val: "-- ", unit: "F" },
          { label: "Target to flip", val: "-- ", unit: "F" },
        ],
        value: 0,
      },
    ],
    notification: {
      title: "It's time to flip your steak!",
      message: "Side one has finished cooking. Flip your steak now for best results."
    }
  },
  {
    step: 5,
    disabled: false,
    summary: [
      {
        title: "5. Flip steak",
        desc: [`Use cooking tongs to flip steak.`, `Close lid.`],
      },
      {
        title: "You'll need:",
        desc: [`Cooking tongs`],
      },
    ],
    source: require("./steak_5.png"),
    progressBar: [
      {
        type: 1, // [0]current, [1]target
        prompt: "",
        messages: [
          { label: "Current temp", val: "-- ", unit: "F" },
          { label: "Target temp", val: "-- ", unit: "F" },
        ],
        value: 0,
        showButton: false,
      },
    ],
    notification: {
      title: "It's time to remove your steak from the grill!",
      message: "Your steak has finished cooking. Remove from grill and let rest now for best results."
    }
  },
  {
    step: 6,
    disabled: false,
    summary: [
      {
        title: "6. Remove steak to rest",
        desc: [
          "Use cooking tongs to remove and transfer steak to a plate or cutting board.",
          "Leave the thermometer in the steak to track resting progress and keep the steak juicy & tender.",
        ],
      },
      {
        title: "You'll need:",
        desc: ["Cooking tongs", "Plate or cutting board"],
      },
    ],
    source: require("./steak_6.png"),
    progressBar: [
      {
        type: 0, // control
        prompt: "Step 6 complete",
        showButton: true,
      },
    ],
  },
  {
    step: 7,
    disabled: false,
    summary: [
      {
        title: "7. Add cauliflower",
        desc: [`Use cooking tongs to place seasoned cauliflower steaks on grill.`, `Close lid.`],
      },
      {
        title: "You'll need:",
        desc: [`Cauliflower steaks`, `Cooking tongs`],
      },
    ],
    source: require("./steak_7.png"),
    progressBar: [
      {
        type: 0, // control
        countdownTimer: true,
        status: -1,
        prompt: "Continue cooking",
        showButton: true,
        showMessages: false,
        messages: [{ label: "Countdown to flip", val: "2:59", unit: "", flag: 0 }],
        value: 0,
      },
      {
        type: 2, // countdown timer 8:00
        countdownTimer: true,
        prompt: "",
        messages: [{ label: "Steak resting", val: "8:00", unit: "", flag: 1 }],
        value: 0,
        showButton: true,
      },
    ],
    notification: {
      title: "It's time to flip your cauliflower!",
      message: "Side one has finished cooking. Flip your cauliflower now for best results."
    }
  },
  {
    step: 8,
    disabled: false,
    summary: [
      {
        title: "8. Flip cauliflower",
        desc: [`Use cooking tongs to flip cauliflower steak.`, `Close lid.`],
      },
      {
        title: "You'll need:",
        desc: [`Cooking tongs`],
      },
    ],
    source: require("./steak_8.png"),
    progressBar: [
      {
        type: 2, // countdown timer
        countdownTimer: true,
        status: -1,
        prompt: "",
        messages: [{ label: "Countdown time", val: "2:59", unit: "", flag: 0 }],
        showButton: true,
        value: 0,
      },
      {
        type: 2, // countdown timer
        countdownTimer: true,
        prompt: "",
        messages: [{ label: "Steak resting", val: "4:59", unit: "", flag: 1 }],
        value: 0,
        showButton: true,
      },
    ],
    notification: {
      title: "It's time to add salsa!",
      message: "While cauliflower steaks are on the grill, use spoon to generously coat with salsa. Add salsa now for best results. "
    }
  },
  {
    step: 9,
    disabled: false,
    summary: [
      {
        title: `9. Coat cauliflower with salsa`,
        desc: [
          `While cauliflower steaks are on the grill, use spoon to generously coat with salsa.`,
          `Close lid.`,
        ],
      },
      {
        title: "You'll need:",
        desc: [`Cauliflower steaks`, `Cooking tongs`],
      },
    ],
    source: require("./steak_9.png"),
    progressBar: [
      {
        type: 2, // countdown timer
        countdownTimer: true,
        status: 0,
        prompt: "",
        messages: [{ label: "Countdown time", val: "1:59", unit: "", flag: 0 }],
        value: 0,
        showButton: true,
      },
      {
        type: 2, // countdown timer
        countdownTimer: true,
        prompt: "",
        messages: [{ label: "Steak resting", val: "1:59", unit: "", flag: 1 }],
        value: 0,
        showButton: true,
      },
    ],
    notification: {
      title: "It's time to Plate your meal!",
      message: "Your cauliflower has finished cooking and your steak has finished resting. Plate and enjoy now for best results."
    }
  },
]

const CARD_CONTAINER: ViewStyle = {
  borderRadius: 20,
  shadowRadius: 10,
  shadowColor: 'rgba(0, 0, 0, 0.5)',
  elevation: 8,
}
const CARD_COVER_CONTAINER: ImageStyle = {
  height: windowHeight / 3.18,
  borderTopLeftRadius: 20,
  borderTopRightRadius: 20,
}
const ITEM_CONTAINER: ViewStyle = {
  marginHorizontal: 7,
  paddingHorizontal: 10,
  marginBottom: 30,
}
const CONTROL_CONTAINER: ViewStyle = {
  marginTop: 30,
  marginHorizontal: 27,
  // borderWidth: 1,
  // borderColor: color.primary
}
const PROGRESS_BAR_CONTAINER: ViewStyle = {
  alignItems: "center",
  // justifyContent: "flex-start"
}
const CONTROL: ViewStyle = {
  flexDirection: "row",
  alignItems: "center",
  justifyContent: "space-between",
}
const TEXT_LABEL: TextStyle = { fontSize: 16, fontFamily: "Gotham-Book", color: "#7A7A7A" }
const TEXT_LABEL_BOLD: TextStyle = { fontSize: 16, fontFamily: "Gotham-Bold", color: color.palette.titleColor }
const GRAYSCALE_STYLE: ViewStyle = {
  opacity: 0.5,
}
const END_VALUE = 1
const COOKING_STEPS_CHANNEL = "cookingSteps"
const MESSAGE_CHANNEL = "message"

const timeToSecond = (time) => {
  const [min, sec] = time.split(":")
  return Number(min * 60) + Number(sec)
}

const secondToTime = (s: number) => {
  let t = ""
  if (s > -1) {
    const min = Math.floor(s / 60) % 60
    const sec = s % 60
    if (min < 10) {
      t += "0"
    }
    t += min + ":"
    if (sec < 10) {
      t += "0"
    }
    t += sec.toFixed(0)
  }
  return t
}

export const CookScreen = observer(function CookScreen() {
  const itemRefs = React.useRef([])
  const scrollViewRef = React.useRef(undefined)
  const breakTimeRef = React.useRef({ val: undefined, end: undefined })
  const [sliderValue, setSliderValue] = React.useState(4)

  const [visible, setVisible] = React.useState(false)
  const showModal = () => setVisible(true)
  const hideModal = () => setVisible(false)

  const [lidOpen, setLidOpen] = React.useState(false)
  const showLidModal = () => setLidOpen(true)
  const hideLidModal = () => setLidOpen(false)

  const [cookSettings] = useState(COOKBOOK_SUMMARY)
  const newCookingSteps = cloneDeep(COOKING_STEPS)
  const [cookingSteps, setCookingSteps] = useState(newCookingSteps)
  const currentStepRef = useRef(0)
  const [startStatus, setStartStatus] = useState(false)
  const [enjoyStatus, setEnjoyStatus] = useState(true)

  const [switchIotMode, setSwitchIotMode] = useState(false)

  useEffect(() => {
    console.log(`sliderValue`, sliderValue)
  }, [sliderValue])

  useEffect(() => {
    // {index: {timeout: id, countdown: seconds}}
    const interval = {}
    const countdownTimerSubs = []
    for (let i = 0; i < cookingSteps.length; i++) {
      const newItem = cookingSteps[i]
      for (let j = 0; j < newItem.progressBar.length; j++) {
        const progressBarItem = newItem.progressBar[j]
        if (progressBarItem.type === 2) {
          const channel = `countdownTimer_${newItem.step - 1}`
          console.log(channel)
          const subscription = DeviceEventEmitter.once(
            channel,
            (data) => {
              console.log(data)
              const index = data.index
              const completeList = []
              const id = setInterval(() => {
                setCookingSteps((oldItems) => {
                  const newItems = [...oldItems]
                  for (let i = 0; i < newItems[index].progressBar.length; i++) {
                    const progressBarItem = newItems[index].progressBar[i]
                    const intervalItem = interval[index]

                    if (completeList.length > 0 && completeList.indexOf(0) !== -1) {
                      console.log(`countdown timer finish. ${index} ${JSON.stringify(completeList)}`)
                      if (index === (cookingSteps.length - 1)) {
                        if (completeList.length === newItems[index].progressBar.length) {
                          clearInterval(intervalItem.timer)
                          onNextStep(newItems[index].step)

                          // if (newItems[index].notification) PushNotification.localNotification(newItems[index].notification)
                        }
                      } else {
                        clearInterval(intervalItem.timer)
                        onNextStep(newItems[index].step)

                        // if (newItems[index].notification) PushNotification.localNotification(newItems[index].notification)
                      }
                    }

                    if (progressBarItem.countdownTimer) {
                      for (let j = 0; j < progressBarItem.messages.length; j++) {
                        const item = progressBarItem.messages[j]
                        let seconds
                        let countdown
                        const intervalItemKey = `countdown_${i}`
                        if (Object.keys(intervalItem).indexOf(intervalItemKey) !== -1) {
                          countdown = intervalItem[intervalItemKey]
                          seconds = timeToSecond(item.val)
                          if (seconds <= 0) {
                            if (completeList.indexOf(i) === -1) completeList.push(i)
                            break
                          }
                        } else {
                          let initialTime
                          for (let k = 0; k < data.progress.length; k++) {
                            const progress0 = data.progress[k]
                            if (progress0.level === i) initialTime = progress0.time
                          }
                          seconds =
                            initialTime && initialTime > 0 ? initialTime : timeToSecond(item.val)

                          if (item.flag === 1) {
                            if (breakTimeRef.current.end === undefined) {
                              breakTimeRef.current.end = item.val
                            }
                            countdown = timeToSecond(breakTimeRef.current.end)
                            intervalItem[intervalItemKey] = countdown
                          } else {
                            intervalItem[intervalItemKey] = seconds
                            countdown = seconds
                          }
                        }
                        if (progressBarItem.status === undefined || progressBarItem.status === 0) {
                          seconds -= 1
                          const time = secondToTime(seconds)
                          item.val = time
                          if (item.flag === 1) {
                            breakTimeRef.current.val = item.val
                          }
                          const result = (countdown - seconds) / countdown
                          if (!isNaN(result)) {
                            progressBarItem.value = result
                            const val0 = progressBarItem.value.toFixed(2)
                            console.log(`${item.label} ${i} time: ${time}, value: ${val0}`)
                          } else {
                            // console.log(`***result***: ${item.label} ${i} (${countdown} - ${seconds}) / ${countdown}`)
                            progressBarItem.value = 1
                          }
                        }

                        if ([6, 7, 8].indexOf(index)) {
                          console.log(progressBarItem)
                        }
                      }
                    } else if (progressBarItem.showButton && !intervalItem.index) {
                      intervalItem.index = i
                    }
                  }
                  return newItems
                })
              }, 1000 * 1)
              interval[index] = { timer: id }
            },
            [],
          )
          countdownTimerSubs.push(subscription)
          break
        }
      }
    }

    const subscription = DeviceEventEmitter.addListener(COOKING_STEPS_CHANNEL, (data) => {
      // {step: 1, progress: [{val: 8, time: 60, target: 145]}}
      console.log("received step:" + data.step)
      if (!data || !data.step) return
      const index = data.step - 1
      if (index < 0) return
      console.log(`currentStep: ${currentStepRef.current}, index: ${index}, ${JSON.stringify(data.progress)}`)
      const newItems = [...cookingSteps]
      const countdownTimer = { index: index, progress: [] }

      // progressBar
      for (let i = 0; i < data.progress.length; i++) {
        const progressItem = data.progress[i]
        const progressBarItem = newItems[index].progressBar[i]
        if (progressBarItem.type === 0) {
          // next control
          if (progressBarItem.endValue) {
            progressBarItem.value = progressItem.val / progressBarItem.endValue
            if (progressBarItem.messages) {
              for (let o = 0; o < progressBarItem.messages.length; o++) {
                const msg = progressBarItem.messages[o]
                if (msg.unit === "%") {
                  msg.val = (progressBarItem.value * 100).toFixed(0)
                } else if (msg.unit === "F" && progressItem.val) {
                  msg.val = progressItem.val
                }
              }
            }
            if (progressBarItem.value === END_VALUE) {
              if (newItems[index].progressBar.length === 1) {
                if (!progressBarItem.showButton) {
                  setTimeout(() => {
                    onNextStep(data.step)
                  }, 1000 * 1.5)
                }
              } else if (newItems[index].progressBar.length === 2) {
                console.log(`step: ${index}`)
                if (newItems[index].type && newItems[index].type === 1) {
                  // hideProgress step 3
                  if (newItems[index].progressBar[0].hideProgress) {
                    setTimeout(() => {
                      onNextStep(data.step)
                    }, 1000 * 1.5)
                  } else {
                    newItems[index].progressBar[1].hideProgress = true
                  }
                } else {
                  const progressBarItem0 = newItems[index].progressBar[0]
                  if (progressBarItem0 && progressBarItem0.disabled) {
                    progressBarItem0.disabled = false
                  }
                }
              }
            }
          } else if (progressBarItem.showMessages) {
            console.log(`progressBarItem.showMessages`, progressBarItem)
            // currentStep: 4, index: 3, [{"val":150,"target":150}]
            const length = progressBarItem.messages.length
            if (length === 2) {
              progressBarItem.messages[0].val = progressItem.val
              progressBarItem.messages[1].val = progressItem.target
                ? progressItem.target
                : progressBarItem.messages[1].val
              progressBarItem.value = progressBarItem.messages[0].val / progressBarItem.messages[1].val
              if (progressBarItem.value === END_VALUE) {
                onNextStep(data.step)
              }
            }
          }
        } else if (progressBarItem.type === 1) {
          // current, target
          const length = progressBarItem.messages.length
          if (length !== 2) return
          progressBarItem.messages[0].val = progressItem.val
          progressBarItem.messages[1].val = progressItem.target
            ? progressItem.target
            : progressBarItem.messages[1].val
          progressBarItem.value = progressBarItem.messages[0].val / progressBarItem.messages[1].val
          if (progressBarItem.value === END_VALUE) {
            if (!progressBarItem.showButton) {
              onNextStep(data.step)
            }
          }
        } else if (progressBarItem.type === 2) {
          // timer
          // console.log(`countdownTimer_${index}s`)
          // countdownTimer.progress.push({ level: i, time: progressItem.time })
        }

        if (progressBarItem.countdownTimer) {
          countdownTimer.progress.push({ level: i, time: progressItem.time })
        }
      }

      if (countdownTimer.progress.length > 0) {
        const channel = `countdownTimer_${index}`
        DeviceEventEmitter.emit(channel, countdownTimer)
      }

      setCookingSteps(newItems) // Update Cooking Steps
    })

    const messageSubscription = DeviceEventEmitter.addListener(MESSAGE_CHANNEL, (data) => {
      console.log(data + "previous lidOpen:" + lidOpen)
      // {status: 1000, step: 7}
      // {status: 1000, step: 7, time: 120}
      console.log("data" + data + " status:" + data.status)
      if (!data || !data.status) {
        console.log("data error")
        return
      }
      if (data.status === 1000) {
        showLidModal()
        console.log("show lid")
      } else if (data.status === 1001) {
        hideLidModal()
        console.log("hide lid")
      }

      setCookingSteps((oldItems) => {
        const newItems = [...oldItems]
        const index = data.step - 1
        if (newItems[index]) {
          const progressBarItem = newItems[index].progressBar[0]
          if (progressBarItem.countdownTimer) {
            if (data.status === 1002) {
              progressBarItem.status = -1
            } else if (data.status === 1003) {
              progressBarItem.status = 0
              let time = timeToSecond(progressBarItem.messages[0].val)
              if (data.time < time) time = data.time
              progressBarItem.messages[0].val = secondToTime(time)
            }
          }
        }
        return newItems
      })
    })

    const loadFunc = async () => {
      const value = await load("SWITCH_IOT")
      if (value && value.switchIot) {
        setSwitchIotMode(value.switchIot)
      }
    }
    loadFunc()

    setTimeout(() => {
      // DeviceEventEmitter.emit("countdownTimer", { index: 6 })
    }, 1000 * 10)

    console.log(COOKING_STEPS[2].progressBar[1])
    console.log(newCookingSteps[2].progressBar[1])
    console.log(cookingSteps[2].progressBar[1])

    return () => {
      console.log(`cook component Will UnMount _____________________`)
      subscription.remove()
      messageSubscription.remove()

      for (const sub of countdownTimerSubs) {
        sub.remove()
      }

      for (const index of Object.keys(interval)) {
        console.log(index)
        clearInterval(interval[index].timer)
      }
    }
  }, [])

  // Pull in one of our MST stores
  // const { someStore, anotherStore } = useStores()

  // Pull in navigation via hook
  const navigation = useNavigation()

  const scrollToItem = (index) => {
    console.log(`scroll to item index: ${index}`)
    // scroll to item
    if (index === undefined) return
    const refToScroll = itemRefs.current[index]
    const cookItem = cookingSteps[index]
    currentStepRef.current = cookItem.step
    console.log(`currentStepRef.current: ${currentStepRef.current}`)
    if (refToScroll === null) return
    refToScroll.measureLayout(findNodeHandle(scrollViewRef.current), (x, y) => {
      console.log(`refToScroll: x:${x}, y:${y}`)
      if (x === 0 && y === 0) return
      scrollViewRef.current.scrollTo({ x: 0, y: y - 25, animated: true })
    })
  }

  const onSwitchButton = (step) => {
    console.log(`onSwitchButton ****`)

    setCookingSteps((oldItems) => {
      const newItems = [...oldItems]
      const index = step - 1
      console.log(index, step)
      console.log(newItems[index].progressBar)
      if (newItems[index].type === 1) {
        if (newItems[index].progressBar.length === 2) {
          console.log(`#####newItems[index].progressBar[1]#####`, newItems[index].progressBar[1])
          if (newItems[index].progressBar[1].hideProgress) {
            onNextStep(step)
          } else {
            newItems[index].progressBar[0].hideProgress = true
          }
        }
      }

      for (let i = 0; i < newItems[index].progressBar.length; i++) {
        const progressBarItem = newItems[index].progressBar[i]
        if (progressBarItem.showMessages === false) {
          progressBarItem.showMessages = true
        }

        if (progressBarItem.showButton) {
          progressBarItem.showButton = false
        }
      }
      return newItems
    })

    if (step === 4) {
      console.log("set second recipe ,sliderValue:" + sliderValue)
      let doneness = "med rare 4"
      switch (sliderValue) {
        case 1:
          doneness = "rare1"
          break
        case 2:
          doneness = "rare2"
          break
        case 3:
          doneness = "medrare3"
          break
        case 4:
          doneness = "medrare4"
          break
        case 5:
          doneness = "med5"
          break
        case 6:
          doneness = "med6"
          break
        case 7:
          doneness = "medwell7"
          break
        case 8:
          doneness = "medwell8"
          break
        case 9:
          doneness = "well9"
          break
        default:
          break
      }
      if (switchIotMode) {
        cloudOperation.setRecipe("rib eye", doneness, 5, 600, -1)
      }
    }
    if (step === 7) {
      console.log("set recipe flower:" + step)
      if (switchIotMode) {
        cloudOperation.setRecipe("cauliflower", "na", 5, 600, -1)
      }
    }
  }

  const onNextStep = (step: number) => {
    console.log(`onNextStep: ${step}`)

    if (step === 0) {
      setStartStatus(!startStatus)
    } else if (cookingSteps.length === step) {
      console.log(`Enjoy!`)
      setEnjoyStatus(false)

      setCookingSteps((oldItems) => {
        const newItems = [...oldItems]
        for (let i = 0; i < newItems.length; i++) {
          newItems[i].disabled = false
        }
        return newItems
      })
      return
    }
    console.log(`switchIotMode: W${switchIotMode}`)
    const loadFunc = async () => {
      const value = await load("SWITCH_IOT")
      if (!value || !value.switchIot) {
        if ([1].indexOf(step) !== -1) {
          setTimeout(() => {
            for (let i = 7; i < 8; i++) {
              setTimeout(() => {
                DeviceEventEmitter.emit(COOKING_STEPS_CHANNEL, {
                  step: step + 1,
                  progress: [{ val: i + 1 }],
                })
              }, 1000 * 1.5)
            }
          }, 1000 * 3)
        } else if ([3, 4].indexOf(step) !== -1) {
          let tempList = [76, 78, 80, 90, 100, 105, 110]
          if (step === 4) {
            tempList = [112, 115, 118, 120, 125]
          }
          const target = tempList[tempList.length - 1]
          for (let i = 0; i < tempList.length; i++) {
            setTimeout(() => {
              DeviceEventEmitter.emit(COOKING_STEPS_CHANNEL, {
                step: step + 1,
                progress: [{ val: tempList[i], target: target }],
              })
            }, 1000 * (i + 3))
          }
        } else if ([6].indexOf(step) !== -1) {
          for (let i = 7; i < 8; i++) {
            setTimeout(() => {
              DeviceEventEmitter.emit(COOKING_STEPS_CHANNEL, {
                step: step + 1,
                progress: [{ val: i + 1, time: 120 }],
              })
            }, 1000 * 1.5)
          }

          setTimeout(() => {
            DeviceEventEmitter.emit(MESSAGE_CHANNEL, { step: 7, status: 1002 })
          }, 1000 * 10)
          setTimeout(() => {
            DeviceEventEmitter.emit(MESSAGE_CHANNEL, { step: 7, status: 1003, time: 30 })
          }, 1000 * 20)
        } else if ([2].indexOf(step) !== -1) {
          const list = [1, 1.8, 2, 5, 5.5, 7, 8]
          for (let i = 0; i < list.length; i++) {
            setTimeout(() => {
              DeviceEventEmitter.emit(COOKING_STEPS_CHANNEL, {
                step: step + 1,
                progress: [{}, { val: list[i] }],
              })
            }, 1000 * (i + 1 + 5))
          }
        }
        switch (step) {
          case 7:
            DeviceEventEmitter.emit(COOKING_STEPS_CHANNEL, {
              step: step + 1,
              progress: [{ time: 60 * 3 }],
            })

            setTimeout(() => {
              DeviceEventEmitter.emit(MESSAGE_CHANNEL, { step: 8, status: 1003, time: 30 })
            }, 1000 * 20)
            break
        }
      }
    }
    loadFunc()

    switch (step) {
      case 8:
        DeviceEventEmitter.emit(COOKING_STEPS_CHANNEL, {
          step: step + 1,
          progress: [{ time: 60 * 2 }],
        })
        cloudOperation.setNowStep(11)
        break
      case 6:
        DeviceEventEmitter.emit(COOKING_STEPS_CHANNEL, {
          step: step + 1,
          progress: [{ time: 60 * 3 }],
        })

        break
      case 7:
        DeviceEventEmitter.emit(COOKING_STEPS_CHANNEL, {
          step: step + 1,
          progress: [{ time: 60 * 3 }],
        })
        break
    }

    setCookingSteps((oldItems) => {
      const newItems = [...oldItems]
      for (let i = 0; i < newItems.length; i++) {
        if (i === step) {
          newItems[i].disabled = true
        } else {
          newItems[i].disabled = false
        }

        if (breakTimeRef.current.val !== undefined) {
          for (let j = 0; j < newItems[i].progressBar.length; j++) {
            const progressBarItem = newItems[i].progressBar[j]
            if (progressBarItem.countdownTimer) {
              for (let k = 0; k < progressBarItem.messages.length; k++) {
                const msg = progressBarItem.messages[k]
                if (msg.flag === 1) {
                  msg.val = breakTimeRef.current.val
                }
              }
            }
          }
        }
      }
      return newItems
    })

    setTimeout(() => {
      scrollToItem(step)
    }, 1000 * 0.3)
  }

  const StepItem = ({ item, index }) => {
    return (
      <View
        key={index}
        style={[
          ITEM_CONTAINER,
          {},
          item.disabled ? {} : GRAYSCALE_STYLE,
          index === 0 ? { marginTop: 30 } : {},
        ]}
        ref={(ref) => (itemRefs.current[index] = ref)}>
        <Card style={[
          CARD_CONTAINER, {},
          // item.disabled ? {} : GRAYSCALE_STYLE,
        ]}>
          {item.source ? (
            <Card.Cover
              style={[CARD_COVER_CONTAINER, {}]}
              source={item.source}
            />
          ) : null}
          <Card.Content
            style={{ marginHorizontal: 11, marginTop: 31, marginBottom: 26 }}
          >
            {item.summary.map((info, index1) => {
              return (
                <View
                  key={index1}
                  style={[
                    index1 === 0 ? { marginTop: 0 } : { marginTop: 21 },
                    {},
                  ]}
                >
                  <Text style={[TEXT_LABEL_BOLD, index1 === 0 ? { fontSize: 21 } : {}]}>
                    {info.title}
                  </Text>
                  {info.desc.map((_d, index2) => {
                    return (
                      <Text
                        style={[TEXT_LABEL, index1 === 0 ? { marginTop: 27 } : {}]}
                        key={index2}
                      >
                        {_d}
                      </Text>
                    )
                  })}
                </View>
              )
            })}
            {item.helper ? (
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  alignItems: "center",
                  marginTop: 30,
                }}
              >
                <Text style={TEXT_LABEL_BOLD}>{item.helper.title}</Text>
                <TouchableOpacity onPress={() => showModal()}>
                  <Image source={require("./helper.png")} />
                </TouchableOpacity>
              </View>
            ) : null}
          </Card.Content>
        </Card>
        {item.progressBar && item.disabled &&
          item.progressBar.map((progressBar, index) => {
            return (
              <View
                key={index}
                style={[
                  CONTROL_CONTAINER,
                  // item.progressBar.length === (index + 1) ? { marginBottom: 10 } : {},
                  index !== 0 ? { marginTop: 22 } : {}, // progressBar+
                  progressBar.hideProgress ? { marginTop: 0 } : {}
                ]}
              >
                {progressBar.type === 0 && !progressBar.hideProgress ? (
                  <Fragment>
                    {!progressBar.showMessages ? <View
                      style={[
                        CONTROL,
                        progressBar.value !== undefined && !progressBar.showButton ? { marginBottom: PROGRESS_BAR_SPACE } : {},
                        progressBar.value !== undefined && progressBar.showButton ? { marginBottom: PROGRESS_BAR_SPACE1 } : {}
                      ]}>
                      <Text style={[TEXT_LABEL, { fontSize: 14, color: color.palette.lightGrey20 }]}>{progressBar.prompt}</Text>
                      {progressBar.showButton ? (
                        <Button
                          onPress={() => {
                            if (progressBar.showMessages !== undefined || item.type === 1) {
                              onSwitchButton(item.step)
                            } else {
                              onNextStep(item.step)
                            }
                          }}
                          style={[
                            {
                              width: 85,
                            },
                            progressBar.disabled ? {
                              backgroundColor: color.palette.white,
                              borderWidth: 1,
                              borderColor: color.palette.buttonColor
                            } : {}
                          ]}>
                          <Text
                            style={[
                              {
                                color: color.palette.white,
                                textAlign: "center",
                                fontSize: 16,
                                marginTop: -3,
                                fontFamily: "Gotham-Medium"
                              },
                              progressBar.disabled ? { color: color.palette.green } : {}
                            ]}>
                            Next
                          </Text>
                        </Button>
                      ) : null}
                    </View> : null}
                    {/* Current Temp, Target to flip */}
                    {progressBar.showMessages ? <View style={CONTROL}>
                      {progressBar.messages && progressBar.messages.map((obj, index) => {
                        return (
                          <View
                            key={index}
                            style={{
                              flexDirection: "row",
                              justifyContent: "space-between",
                              alignItems: "center",
                              marginBottom: PROGRESS_BAR_SPACE,
                              // borderWidth: 1,
                            }}
                          >
                            <Text style={[TEXT_LABEL, { fontSize: 14, textAlign: "center" }]}>{obj.label}</Text>
                            <Text
                              style={[TEXT_LABEL_BOLD, { fontSize: 14, marginLeft: 5, textAlign: "center", marginTop: 3 }]}
                            >{`${obj.val}${obj.unit}`}</Text>
                          </View>
                        )
                      })}
                    </View> : null}
                    {progressBar.value !== undefined ? <View style={[PROGRESS_BAR_CONTAINER, {}]}>
                      <Progress.Bar
                        color={PROGRESS_BAR_COLOR}
                        borderWidth={0}
                        width={PROGRESS_BAR_WIDTH}
                        height={8}
                        style={{ backgroundColor: color.palette.white }}
                        progress={progressBar.value ? progressBar.value : 0}
                      />
                    </View> : null}
                  </Fragment>
                ) : null}
                {(progressBar.type === 1 || progressBar.type === 2) ? (
                  <Fragment>
                    {/* Current Temp, Target to flip */}
                    <View style={CONTROL}>
                      {progressBar.messages.map((obj, index) => {
                        return (
                          <View
                            key={index}
                            style={{
                              flexDirection: "row",
                              justifyContent: "space-between",
                              alignItems: "center",
                              marginBottom: PROGRESS_BAR_SPACE,
                            }}
                          >
                            <Text style={[TEXT_LABEL, { fontSize: 14, textAlign: "center" }]}>{obj.label}</Text>
                            <Text
                              style={[TEXT_LABEL_BOLD, { fontSize: 14, marginLeft: 5, textAlign: "center", marginTop: 3 }]}
                            >{`${obj.val}${obj.unit}`}</Text>
                          </View>
                        )
                      })}
                    </View>
                    <View style={[PROGRESS_BAR_CONTAINER, {}]}>
                      <Progress.Bar
                        color={PROGRESS_BAR_COLOR}
                        borderWidth={0}
                        width={PROGRESS_BAR_WIDTH}
                        height={8}
                        style={{ backgroundColor: color.palette.white }}
                        progress={progressBar.value ? progressBar.value : 0}
                      />
                    </View>
                  </Fragment>
                ) : null}
              </View>
            )
          })}
      </View >
    )
  }

  return (
    <Screen style={ROOT} preset="scroll">
      <ScrollView ref={(ref) => (scrollViewRef.current = ref)}>
        <CookHeader
          title="Grilled fillet and cauliflower steak"
          titleStyle={{ fontFamily: "Gotham-Bold", fontSize: 21 }}
          onLeftPress={() => navigation.goBack()} />
        <CookSettings
          buttonLabel={"Start recipe"}
          sliderValue={sliderValue}
          onChangeSliderValue={(value) => setSliderValue(value)}
          source={STEAK_IMG}
          style={!startStatus ? {
            borderBottomLeftRadius: 0,
            borderBottomRightRadius: 0
          } : {}}
          grayscale={startStatus}
          contentStyle={{ marginHorizontal: 27 }}
          itemStyle={{ marginHorizontal: 17 }}
          ingredients={cookSettings.ingredients}
          disabled={startStatus}
          onPress={() => { if (switchIotMode) cloudOperation.setRecipe("rib eye", "none", cookSettings.thickness, cookSettings.cookTime, 200); onNextStep(0) }}
        />
        <>
          {startStatus && cookingSteps.map((data, index) => {
            return <StepItem item={data} index={index} key={index} />
          })}
        </>
        {startStatus ? <View style={[{ marginTop: 10 }, enjoyStatus ? GRAYSCALE_STYLE : {}]}>
          <ImageBackground
            style={{
              height: IMAGE_WIDTH,
              width: windowWidth,
              alignItems: "center",
              justifyContent: "center",
            }}
            source={STEAK_IMG}
          >
            {/* onPress={() => navigation.dispatch(StackActions.popToTop())} */}
            <Button
              disabled={enjoyStatus}
              onPress={() => navigation.dispatch(StackActions.replace("bottomTabs"))}
              style={[
                { height: 48, width: windowWidth - 85.5 * 2 },
                { ...style.SHADOW_STYLE },
              ]}>
              <Text style={[
                TEXT_LABEL,
                { color: color.palette.white, fontSize: 19, textAlign: "center" },
              ]}>
                Enjoy!
              </Text>
            </Button>
          </ImageBackground>
        </View> : null}
      </ScrollView>
      <Modal
        title="Tips for thermometer"
        desc="Ensure tip of thermometer is embedded in the most center of the protein."
        source={require("./steak_4_1.png")}
        visible={visible}
        onDismiss={hideModal}
      />
      <Modal
        title="Lid is open"
        desc="Cooking will continue once lid is Closed."
        source={require("./tem.png")}
        visible={lidOpen}
        hideCloseButton={false}
        onDismiss={hideLidModal}
      />
    </Screen >
  )
})
