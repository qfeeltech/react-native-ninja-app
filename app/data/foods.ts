
export interface RecipesProps {
	title: string
	source: any
	time: string
	type: any
}

export interface FoodKeyProps {
	string: string[]
}

export const RECIPES: RecipesProps[] = [
	{
		title: "Garlic butter baked salmon with Greek salad",
		source: require("./assets/images/foods/fish01.png"),
		time: "20 minutes",
		type: "Fish"
	},
	{
		title: "Paprika grilled salmon and spring salad",
		source: require("./assets/images/foods/chicken02.png"),
		time: "40 minutes",
		type: "Fish"
	},
	{
		title: "Vegan Thai peanut stir fry",
		source: require("./assets/images/foods/vegetable01.png"),
		time: "20 minutes",
		type: "Vegetable"
	},
	{
		title: "Buffalo mozzarella and tomato pizza",
		source: require("./assets/images/foods/vegetable02.png"),
		time: "40 minutes",
		type: "Vegetable"
	},
	{
		title: "Roast chicken with sweet potato and quinoa",
		source: require("./assets/images/foods/chicken01.png"),
		time: "35 minutes",
		type: "Chicken"
	},
	{
		title: "Grilled chicken and radish salad",
		source: require("./assets/images/foods/chicken02.png"),
		time: "20 minutes",
		type: "Chicken"
	},
	{
		title: "Honey garlic pork chops with asparagus",
		source: require("./assets/images/foods/pork01.png"),
		time: "30 minutes",
		type: "Pork"
	},
	{
		title: "Yellow curry pork with zucchini noodles",
		source: require("./assets/images/foods/pork02.png"),
		time: "35 minutes",
		type: "Pork"
	},
	{
		title: "Fillet with roasted potatoes and asparagus",
		source: require("./assets/images/foods/beef.png"),
		time: "45 minutes",
		type: "Beef"
	},
	{
		title: "Sirloin with garlic spinach and potatoes",
		source: require("./assets/images/foods/round_steak.png"),
		time: "35 minutes",
		type: "Beef"
	},
	{ title: "Spice crusted fillet with white bean salads", source: require("./assets/images/foods/beef01.png"), time: "25 minutes", type: "Beef" },
	{ title: "T-Bone steak with thyme and garlic butter", source: require("./assets/images/foods/T-bone.png"), time: "20 minutes", type: "Beef" },
	{ title: "Fillet tacos with homemade salsa", source: require("./assets/images/foods/unnamed.png"), time: "35 minutes", type: "Beef" },
	{ title: "Beef and vegetable teriyaki stir fry", source: require("./assets/images/foods/beef02.png"), time: "30 minutes", type: "Beef" },
	{
		title: "Grilled fillet and cauliflower steak",
		source: require("./assets/images/foods/main_image.png"),
		time: "45 minutes",
		type: "Beef"
	},
	{
		title: "Beef brisket with homemade BBQ sauce",
		source: require("./assets/images/foods/Brisket.png"),
		time: "35 minutes",
		type: "Beef"
	},
]

export const FOOD_KEYS = {
	Beef: ["Sirloin", "Fillet", "Flank", "NY Strip", "Ribeye ", "Brisket", "Flat Iron", "T-Bone", "Rib"],
	Fish: ["Tuna", "Salmon", "Trout", "Halibut", "Cod", "Mackerel", "Herring", "Swordfish", "Haddock"],
	Pork: ["Loin", "Rib", "Chops", "Belly", "Shoulder", "Sausage", "Tenderloin", "Bacon", "Ground"],
	Vegetable: ["Broccoli", "Beet", "Pepper", "Carrot", "Kale", "Mushroom", "Bean", "Asparagus", "Cauliflower"],
	Chicken: ["Breast", "Wing", "Thigh", "Drumstick", "Leg", "Whole"],
}