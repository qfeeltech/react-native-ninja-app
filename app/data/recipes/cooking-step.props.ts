export interface CookingStepProps {
  step: number
  summary: {
    title: string
    desc: string[]
  }[]
  source?: any
  disabled?: boolean
  helper?: {
    title: string
    source: any
  }
  type?: number
  progressBar: {
    type: number
    hideProgress?: boolean
    countdownTimer?: boolean
    status?: number
    disabled?: boolean
    direction?: number
    showMessages?: boolean
    prompt?: string
    showButton: boolean
    endValue?: number
    value?: number
    messages?: { label: string; val: any; unit: string, flag?: number }[]
  }[]
  notification?: { title: string, message: string }
}