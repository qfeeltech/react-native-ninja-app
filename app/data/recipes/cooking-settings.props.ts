export interface CookingSettingsProps {
  cookTime: number
  doneness: number
  thickness: number
  source: any
  ingredients: string[]
}