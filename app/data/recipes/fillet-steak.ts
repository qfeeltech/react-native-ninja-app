import { CookingStepProps } from './cooking-step.props'
import { CookingSettingsProps } from './cooking-settings.props'

const filletSteakSteps: CookingStepProps[] = [
  {
    step: 1,
    disabled: false,
    summary: [
      {
        title: "1. Prep steak",
        desc: [
          "Brush each steak on all sides with canola oil.",
          "Season with salt and pepper as desired.",
        ],
      },
      {
        title: "You'll need:",
        desc: ["Fillet steak", "1/2 tablespoon canola oil", "salt and pepper"],
      },
    ],
    source: require("../assets/images/recipes/recipe_01_step_1.png"),
    progressBar: [
      {
        type: 0, // control
        prompt: "Step 1 complete",
        showButton: true,
      },
    ],
  },
  {
    step: 2,
    disabled: false,
    summary: [
      {
        title: "2. Preheat unit",
        desc: ["Press start/stop on unit to start preheating."],
      },
    ],
    progressBar: [
      {
        type: 0, // control
        prompt: "Ready to Preheat",
        showButton: false,
        hideProgress: true,
        value: 0,
        endValue: 8,
      },
    ],
  },
  {
    step: 3,
    disabled: false,
    summary: [
      {
        title: "3. Prep cauliflower and salsa",
        desc: [
          `Cut cauliflower from top to bottom into two 3/4-1 inch "steaks"; reserve remaining cauliflower.`,
          `Season with salt and pepper as desired.`,
          `To make the Greek salsa, in a large bowl, stir together olives, roasted red peppers, oregano, parsley, garlic, lemon juice, feta, salt, pepper, walnuts, red onion, and 2 tablespoons of canola oil.`,
          `Set Aside.`,
        ],
      },
    ],
    source: require("../assets/images/recipes/recipe_01_step_3.png"),
    type: 1,
    progressBar: [
      {
        type: 0, // control
        prompt: "Step 3 complete",
        showButton: true,
      },
      {
        type: 0, // control
        prompt: "Unit preheating",
        showMessages: true,
        messages: [
          { label: "Unit preheating", val: 0, unit: "%" },
        ],
        showButton: false,
        value: 0,
        endValue: 8,
      },
    ],
    notification: {
      title: "Preheat complete!",
      message: "Ready to add steak."
    }
  },
  {
    step: 4,
    disabled: false,
    summary: [
      {
        title: "4. Add steak",
        desc: [
          `Set up thermometer with steak and place both in grill.`,
          `Place steak on grill plate.`,
          `Close lid.`,
        ],
      },
      {
        title: "You'll need:",
        desc: [`Fillet steak`, `Thermometer`],
      },
    ],
    helper: {
      title: "Tips for thermometer",
      source: require("../assets/images/recipes/recipe_01_step_4_1.png"),
    },
    source: require("../assets/images/recipes/recipe_01_step_4_0.png"),
    progressBar: [
      {
        type: 0, // control
        prompt: "Step 4 complete",
        showButton: true,
        direction: 1,
        showMessages: false,
        messages: [
          { label: "Current temp", val: "-- ", unit: "F" },
          { label: "Target to flip", val: "-- ", unit: "F" },
        ],
        value: 0,
      },
    ],
    notification: {
      title: "It's time to flip your steak!",
      message: "Side one has finished cooking. Flip your steak now for best results."
    }
  },
  {
    step: 5,
    disabled: false,
    summary: [
      {
        title: "5. Flip steak",
        desc: [`Use cooking tongs to flip steak.`, `Close lid.`],
      },
      {
        title: "You'll need:",
        desc: [`Cooking tongs`],
      },
    ],
    source: require("../assets/images/recipes/recipe_01_step_5.png"),
    progressBar: [
      {
        type: 1, // [0]current, [1]target
        prompt: "",
        messages: [
          { label: "Current temp", val: "-- ", unit: "F" },
          { label: "Target temp", val: "-- ", unit: "F" },
        ],
        value: 0,
        showButton: false,
      },
    ],
    notification: {
      title: "It's time to remove your steak from the grill!",
      message: "Your steak has finished cooking. Remove from grill and let rest now for best results."
    }
  },
  {
    step: 6,
    disabled: false,
    summary: [
      {
        title: "6. Remove steak to rest",
        desc: [
          "Use cooking tongs to remove and transfer steak to a plate or cutting board.",
          "Leave the thermometer in the steak to track resting progress and keep the steak juicy & tender.",
        ],
      },
      {
        title: "You'll need:",
        desc: ["Cooking tongs", "Plate or cutting board"],
      },
    ],
    source: require("../assets/images/recipes/recipe_01_step_6.png"),
    progressBar: [
      {
        type: 0, // control
        prompt: "Step 6 complete",
        showButton: true,
      },
    ],
  },
  {
    step: 7,
    disabled: false,
    summary: [
      {
        title: "7. Add cauliflower",
        desc: [`Use cooking tongs to place seasoned cauliflower steaks on grill.`, `Close lid.`],
      },
      {
        title: "You'll need:",
        desc: [`Cauliflower steaks`, `Cooking tongs`],
      },
    ],
    source: require("../assets/images/recipes/recipe_01_step_7.png"),
    progressBar: [
      {
        type: 0, // control
        countdownTimer: true,
        status: -1,
        prompt: "Continue cooking",
        showButton: true,
        showMessages: false,
        messages: [{ label: "Countdown to flip", val: "2:59", unit: "", flag: 0 }],
        value: 0,
      },
      {
        type: 2, // countdown timer 8:00
        countdownTimer: true,
        prompt: "",
        messages: [{ label: "Steak resting", val: "8:00", unit: "", flag: 1 }],
        value: 0,
        showButton: true,
      },
    ],
    notification: {
      title: "It's time to flip your cauliflower!",
      message: "Side one has finished cooking. Flip your cauliflower now for best results."
    }
  },
  {
    step: 8,
    disabled: false,
    summary: [
      {
        title: "8. Flip cauliflower",
        desc: [`Use cooking tongs to flip cauliflower steak.`, `Close lid.`],
      },
      {
        title: "You'll need:",
        desc: [`Cooking tongs`],
      },
    ],
    source: require("../assets/images/recipes/recipe_01_step_8.png"),
    progressBar: [
      {
        type: 2, // countdown timer
        countdownTimer: true,
        status: -1,
        prompt: "",
        messages: [{ label: "Countdown time", val: "2:59", unit: "", flag: 0 }],
        showButton: true,
        value: 0,
      },
      {
        type: 2, // countdown timer
        countdownTimer: true,
        prompt: "",
        messages: [{ label: "Steak resting", val: "4:59", unit: "", flag: 1 }],
        value: 0,
        showButton: true,
      },
    ],
    notification: {
      title: "It's time to add salsa!",
      message: "While cauliflower steaks are on the grill, use spoon to generously coat with salsa. Add salsa now for best results. "
    }
  },
  {
    step: 9,
    disabled: false,
    summary: [
      {
        title: `9. Coat cauliflower with salsa`,
        desc: [
          `While cauliflower steaks are on the grill, use spoon to generously coat with salsa.`,
          `Close lid.`,
        ],
      },
      {
        title: "You'll need:",
        desc: [`Cauliflower steaks`, `Cooking tongs`],
      },
    ],
    source: require("../assets/images/recipes/recipe_01_step_9.png"),
    progressBar: [
      {
        type: 2, // countdown timer
        countdownTimer: true,
        status: 0,
        prompt: "",
        messages: [{ label: "Countdown time", val: "1:59", unit: "", flag: 0 }],
        value: 0,
        showButton: true,
      },
      {
        type: 2, // countdown timer
        countdownTimer: true,
        prompt: "",
        messages: [{ label: "Steak resting", val: "1:59", unit: "", flag: 1 }],
        value: 0,
        showButton: true,
      },
    ],
    notification: {
      title: "It's time to Plate your meal!",
      message: "Your cauliflower has finished cooking and your steak has finished resting. Plate and enjoy now for best results."
    }
  },
]

const filletSteakSettings: CookingSettingsProps = {
  cookTime: 40,
  doneness: 5,
  thickness: 1,
  source: require("../assets/images/recipes/recipe_01_desc.png"),
  ingredients: [
    "Fillet steak",
    "1/2 tablespoon canola oil",
    "Cauliflower",
    "1/3 cup olives",
    "1/2 cup roasted red peppers",
    "1 tbsp minced oregano",
    "1 tbsp minced parsley",
    "3 minced garlic cloves",
    "1 lemon (juice)",
    "1lb crumbled feta",
    "Salt and pepper",
  ],
}

export const filletSteak = {
  settings: filletSteakSettings,
  steps: filletSteakSteps,
}