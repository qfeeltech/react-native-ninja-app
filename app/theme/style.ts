import { color } from "."

export const style = {
    SHADOW_STYLE: {
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 1,
        shadowColor: color.palette.shadowColor20,
        shadowRadius: 6,
    },
    IMAGE_SHADOW_STYLE: {
        shadowColor: color.palette.shadowColor20,
        shadowOffset: { width: 0, height: 15 },
        shadowOpacity: 15,
        shadowRadius: 15,
    }
}